﻿
using BotCore.Structures.Base;

using DSharpPlus.Entities;

using System;
using System.Threading.Tasks;

namespace SiliconBotCore3
{
    internal class Program
    {
        [MTAThread]
        private static void Main()
        {
            new Program().RunBotAsync().GetAwaiter().GetResult();
        }
        private async Task RunBotAsync()
        {
            Bot bot = new Bot("Silicon Bot");
            bot.Statusses.AddRange(new[]
            {
                new DiscordActivity ("Respect On Hotel Wi-Fi", ActivityType.Watching),
                new DiscordActivity ("Licking 600VAC")
            });
            bot.Start();
            await Task.Delay(-1);
        }
    }
}
