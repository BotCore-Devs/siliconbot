﻿using BotCore.Structures.Base;
using CoreBot.Structures.Command;

using DSharpPlus.Entities;

using OfficeOpenXml;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using OmniarchiveIndexParser;
using CoreBot.Structures.DataStorage;

namespace Omnibot
{
    public class Program
    {
        [MTAThread]
        public static void Main()
        {
            new Program().RunBotAsync().GetAwaiter().GetResult();
        }
        WebClient wc = new WebClient();

        public string IndexDate { get { return $"Data was last updated on: {new FileInfo("index.json").LastWriteTimeUtc} UTC"; } }
        public async Task RunBotAsync()
        {
            Bot bot = new Bot("Omnibot");
            string basename = "Omnibot";
            Stopwatch swsetup = bot.Timing.StartTiming(basename);
            Dictionary<string, List<McInfo>> index = new Dictionary<string, List<McInfo>>();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            new Thread(() => { index = McUtil.ParseVersionsRewrite(); }).Start();
            bot.Timing.GotoNext(swsetup, $"{basename}.RegisterCommands");

            bot.CommandHandler.RegisterCommand("index", new CommandDefinition(CommandCategory.Minecraft, "Get a link to the Omniarchive index", async (ce) =>
            {
                if (ce.E.Guild.Id != 361634042317111296) await ce.SendMessageAsync("View the full index at <https://derpy.me/omnidex>");
            }));
            bot.CommandHandler.RegisterCommand("lookup", new CommandDefinition(CommandCategory.Minecraft, "Show whether a client or server has been found", async (ce) =>
            {
                if (ce.Args.Count > 3)
                {
                    ce.Args[2] = string.Join(" ", ce.Args.Skip(2));
                    ce.Args.RemoveRange(3, ce.Args.Count - 2);
                }

                Console.WriteLine(ce.Args.Count);
                bool validargs = true;

                if (ce.Args.Count != 3)
                {
                    validargs = false;
                }
                else
                {
                    switch (ce.Args[0].ToLower())
                    {
                        case "j":
                        case "java":
                            ce.Args[0] = "java";
                            break;
                        /*case "b":
                        case "bedrock":
                            ce.Args[0] = "bedrock";
                            break;*/
                        default:
                            validargs = false;
                            break;
                    }
                    switch (ce.Args[1].ToLower())
                    {
                        case "c":
                        case "client":
                        case "clients":
                            ce.Args[1] = "clients";
                            break;
                        case "s":
                        case "server":
                        case "servers":
                            ce.Args[1] = "servers";
                            break;
                        default:
                            validargs = false;
                            break;
                    }
                    try
                    {
                        Console.WriteLine(string.Join(", ", ce.Args));
                        int results = 0;
                        foreach (McInfo ver in index[ce.Args[0] + ce.Args[1]])
                        {
                            if (ver.Id == ce.Args[2] || ver.Version == ce.Args[2])
                            {

                                //await ce.SendMessageAsync($"Found possible match: ```json\n{JsonConvert.SerializeObject(ver, Formatting.Indented)}```"/*, embed: embed.Build()*/);
                                if (results++ < 3)
                                {
                                    await ce.SendMessageAsync(IndexDate, embed: ver.GetVersionEmbed());
                                }
                            }
                        }
                        if (results == 0)
                        {
                            await ce.SendMessageAsync("No results found, if this is in error according to the index (visible at <https://derpy.me/omnidex>), report this to The Arcane Brony!\n" + IndexDate);
                        }
                        else
                        {
                            await ce.SendMessageAsync($"Found {results} results! If anything shows up that didn't even match your query, report this to The Arcane Brony!\n" + IndexDate);
                        }
                    }
                    catch (KeyNotFoundException) { await ce.SendMessageAsync($"Invalid type args have been passed, proper error coming some day..."); }
                    catch (Exception err) { await ce.SendMessageAsync($"[<@84022289024159744>] An error ocurred ```csharp\n{err}```"); }
                }
                if (!validargs)
                {
                    await ce.SendMessageAsync($"Syntax: `{ce.Server.Prefix}lookup <edition> <type> <version>`\nEditions: `Java`, ~~`Bedrock`,~~ `J`, ~~`B`~~\nTypes: `Client`, `Server`, `Clients`, `Servers`, `C`, `S`\n*These are not case sensitive*\n\nGiven args: {string.Join(", ", ce.Args)}");
                }
            }));
            bot.CommandHandler.RegisterCommand("pvn", new CommandDefinition(CommandCategory.Minecraft, "List of Minecraft servers and clients that use this protocol version number", async (ce) =>
            {
                try
                {
                    List<string> clients = new List<string>(), servers = new List<string>();
                    foreach (McInfo ver in index["javaclients"])
                    {
                        if (ver.Pvn == ce.Args[0])
                        {
                            clients.Add(ver.Id);
                        }
                    }
                    foreach (McInfo ver in index["javaservers"])
                    {
                        if (ver.Pvn == ce.Args[0])
                        {
                            servers.Add(ver.Id);
                        }
                    }
                    await ce.SendMessageAsync($"Clients: {string.Join(", ", clients)}\nServers: {string.Join(", ", servers)}\n{IndexDate}");
                }
                catch (KeyNotFoundException) { await ce.SendMessageAsync($""); }
                catch (Exception err) { await ce.SendMessageAsync($"[<@84022289024159744>] An error has ocurred ```csharp\n{err}```"); }

            }));
            bot.CommandHandler.RegisterCommand("archive", new CommandDefinition(CommandCategory.Minecraft, "Get the archive.org links", async (ce) =>
            {
                if (ce.E.Guild.Id != 361634042317111296) await ce.SendMessageAsync("Archive.org links:\n\nPre-Classic: <https://archive.org/details/Minecraft-JE-Pre-Classic>\nClassic: <https://archive.org/details/Minecraft-JE-Classic>\nIndev: <https://archive.org/details/Minecraft-JE-Indev>\nInfdev: <https://archive.org/details/Minecraft-JE-Infdev>\nAlpha: <https://archive.org/details/Minecraft-JE-Alpha>\nBeta: <https://archive.org/details/Minecraft-JE-Beta>");
            }));

            bot.CommandHandler.RegisterCommand("debugindex", new CommandDefinition(CommandCategory.Developer, "Check how the bot read the index", async (ce) =>
            {
                await ce.SendFileAsync("index.json", $"Here's how I interpreted the index!\n{IndexDate}");
            }));
            bot.CommandHandler.RegisterCommand("identify", new CommandDefinition(CommandCategory.Minecraft, "Identify a Minecraft version based on hash(es) or attachment(s) (Latter is DM only)", async (ce) =>
            {
                bool found = false;
                if (ce.E.Message.Attachments.Count != 0 && ce.E.Guild == null)
                {
                    using (MD5 md5 = MD5.Create())
                    {
                        foreach (DiscordAttachment att in ce.E.Message.Attachments)
                        {
                            byte[] hashed = md5.ComputeHash(wc.DownloadData(att.Url));
                            var hash = BitConverter.ToString(hashed).Replace("-", "");

                            await ce.SendMessageAsync($"The MD5 for *{att.FileName}* is `{hash}`");
                            foreach (List<McInfo> info in index.Values)
                            foreach (McInfo mcinfo in info)
                                if (mcinfo.Md5 == hash)
                                {
                                    await ce.SendMessageAsync(IndexDate, embed: mcinfo.GetVersionEmbed());
                                    found = true;
                                }
                        }
                    }
                }
                foreach (string arg in ce.Args)
                foreach (List<McInfo> info in index.Values)
                {
                    foreach (McInfo mcinfo in info)
                    {
                        if (mcinfo.Sha256 == arg || mcinfo.Md5 == arg)
                        {
                            await ce.SendMessageAsync(IndexDate, embed: mcinfo.GetVersionEmbed());
                            found = true;
                        }
                    }
                }
                if (!found) await ce.SendMessageAsync("No version(s) found matching the criteria!\n" + IndexDate);
            }));
            bot.CommandHandler.RegisterCommand("notificationsquad", new CommandDefinition(CommandCategory.Discord, "Get Notification Squad role", async (ce) =>
            {
                if (!((DiscordMember)ce.E.Author).Roles.Contains(ce.E.Guild.GetRole(635583048762523650)))
                {
                    await ((DiscordMember)ce.E.Author).GrantRoleAsync(ce.E.Guild.GetRole(635583048762523650));
                    await ce.SendMessageAsync("You have been added to the Notification Squad!");
                }
                else
                {
                    await ((DiscordMember)ce.E.Author).RevokeRoleAsync(ce.E.Guild.GetRole(635583048762523650));
                    await ce.SendMessageAsync("You have been removed from the Notification Squad!");
                }
            }));
            bot.CommandHandler.RegisterCommand("voicesquad", new CommandDefinition(CommandCategory.Discord, "Get Voice Squad role", async (ce) =>
            {
                if (!((DiscordMember)ce.E.Author).Roles.Contains(ce.E.Guild.GetRole(664496426738253824)))
                {
                    await ((DiscordMember)ce.E.Author).GrantRoleAsync(ce.E.Guild.GetRole(664496426738253824));
                    await ce.SendMessageAsync("You have been added to the Voice Squad!");
                }
                else
                {
                    await ((DiscordMember)ce.E.Author).RevokeRoleAsync(ce.E.Guild.GetRole(664496426738253824));
                    await ce.SendMessageAsync("You have been removed from the Voice Squad!");
                }
            }));
            //override settings for DMs
            LegacyServer DM = (await bot.DataStore.GetServer(0));
            DM.Prefix = ".";
            DM.Save();
            //override settings for OmniArchive
            LegacyServer OmniArchive = (await bot.DataStore.GetServer(361634042317111296));
            OmniArchive.Prefix = ".";
            OmniArchive.UnknownCommandMessage = false;
            OmniArchive.LevelUpMessagesEnabled = false;
            OmniArchive.DisabledCommandMessage = false;
            OmniArchive.ModerationEnabled = false;
            OmniArchive.RaidDefenseEnabled = false;
            OmniArchive.ResetOnLeave = false;
            OmniArchive.Save();
            bot.Timing.StopTiming(swsetup);
            /*if (bot.Name != "Omnibot") foreach (KeyValuePair<string, CommandDefinition> command in bot.commandHandler.commands)
                {
                    if (command.Value.Category == CommandCategory.Music
                        || command.Key == "lplay"
                        || command.Key == "debuginfo"
                        || command.Key == "serverinfo"
                        || command.Key == "userinfo"
                        || command.Value.Category == CommandCategory.Leveling
                        || (command.Value.Category == CommandCategory.Fun && command.Key != "avatar")
                        || (command.Value.Category == CommandCategory.Moderation && command.Key != "config" && command.Key != "disablecmd"))
                    {
                        bot.commandHandler.commands.Remove(command.Key);
                        Console.WriteLine($"Removing command [{command.Value.Category}] {command.Key}");
                    }
                }*/
            bot.Start();
            await Task.Delay(-1);
        }
    }
}
