﻿using CoreBot.EventHandlers.Commands;

using System.Linq;
using DSharpPlus;
using DSharpPlus.Entities;

namespace CoreBot
{
    internal class Checks
    {
        public static bool IsStaff(CommandEnvironment ce)
        {
            return ce.Server.GuildId == 0 ? IsBotOwner(ce) : ce.Server.Roles.Mod != 0 ? 
                ce.E.Member().Roles.Contains(ce.E.Guild.GetRole(ce.Server.Roles.Mod)) 
                : ce.Server.Roles.Admin != 0 ?
                    ce.E.Member().Roles.Contains(ce.E.Guild.GetRole(ce.Server.Roles.Admin)) 
                    : IsGuildOwner(ce) ||
                        IsBotOwner(ce);
        }

        public static bool IsStaff(DiscordClient client, ulong guildId, ulong userId)
        {
            if (guildId == 0 || userId == 0) return false;
            DiscordGuild guild = client.Guilds.ContainsKey(guildId)
                ? client.Guilds[guildId]
                : client.GetGuildAsync(guildId).Result;
            //if (guild.GetWebhooksAsync().Result.Any(x => x.Id == userId)) return false;
            try
            {
                DiscordMember user = guild.Members.ContainsKey(userId)
                ? guild.Members[userId]
                : guild.GetMemberAsync(userId).Result;
                return IsGuildOwner(guild, user) || IsBotOwner(user) || user.PermissionsIn(guild.GetDefaultChannel())
               .HasPermission(Permissions.KickMembers);
            }
            catch {
                return false;
            }
        }
        public static bool IsBotOwner(CommandEnvironment ce)
        {
            return ce.E.Author.Id == 84022289024159744;
        }

        public static bool IsBotOwner(DiscordUser user)
        {
            return user.Id == 84022289024159744;
        }
        public static bool IsGuildOwner(CommandEnvironment ce)
        {
            return ce.E.Guild.Owner.Id == ce.E.Author.Id;
        }

        public static bool IsGuildOwner(DiscordGuild guild, DiscordUser user)
        {
            return guild.Owner.Id == user.Id;
        }
        public static bool IsBotDeveloper(CommandEnvironment ce)
        {
            return IsBotOwner(ce) || ce.E.Author.Id == 289884287765839882 || ce.E.Author.Id == 496071519567609867 || ce.E.Author.Id == 289884287765839882;
        }
        public static bool UserAffectableByModeration(CommandEnvironment ce)
        {
            return ce.Author.UserId != 84022289024159744;
        }
    }
}
