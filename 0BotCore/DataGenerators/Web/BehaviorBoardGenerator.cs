﻿using BotCore.Structures.Base;

using CoreBot.Structures.DataStorage;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace BotCore.DataGenerators.Web
{
    public class BehaviorBoardGenerator
    {
        private static readonly string Basepage = @"<meta charset='UTF-8'><style>
  .pb {
    height: 25px;
    background-color: black;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
    border-radius: 25px;
}
.pb > span {
    display: block;
    height: 100%;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
border-radius: 25px;
    background-color: blue;
    position: relative;
}
body {background-color:#242424; color:#ffffff; font-family:'Segoe UI';}
name {font-size:20; vertical-align:middle;}
level {font-size:20; vertical-align:middle;}
discrim {font-size:10; vertical-align:middle}
user {width:500px; display:inline-block; background-color:#a2a2a20f; border-radius:25px; padding-top:20px; margin:5px;}
#avatar {height:64px; border-radius: 50%; vertical-align: middle; }
</style>
<script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>
<script>
    var loc = window.location.href;
    if(!loc.endsWith('/')) loc+='/';
window.addEventListener('error', function(e) {
var uri = loc + e.target.accessKey + '/UpdateAvatar';
console.log(uri);
    $.get(uri, function (data, textStatus, jqXHR) { 
    e.target.outerHTML = data;
})
}, true);
setInterval(()=>$('#content>center').load(loc+'update'), 250)</script>
<meta charset='UTF-8'>
<center>
    <div>";
        public string Generate(ulong guildId)
        {

            string basename = "GenBehaviorBoard_" + guildId + "_" + DateTime.Now.Ticks;
            Stopwatch timer = bot.Timing.StartTiming(basename);
            LegacyServer server = bot.DataStore.GetServer(guildId).Result;
            List<LegacyUser> susers = new List<LegacyUser>();
            susers = server.EnumUsers().Where(x => x != null && x.ContentFilterData.Total >= 1).ToList();
            susers.Sort(delegate (LegacyUser y, LegacyUser x)
            {
                int xdiff = x.ContentFilterData.Total.CompareTo(y.ContentFilterData.Total);
                if (xdiff != 0)
                {
                    return xdiff;
                }
                else
                {
                    return x.Xp.CompareTo(y.Xp);
                }
            });

            string scoreboard = @"";
            scoreboard = Basepage.Replace(".html", "");

            foreach (LegacyUser user in susers)
            {
                scoreboard += GetUserCard(user);
            }
            scoreboard += $"\n</div>\n    </center>";

            bot.Timing.StopTiming(timer);
            return scoreboard;
        }
        public string GenerateContent(ulong guildId)
        {
            string basename = "GenBehaviorBoardContent_" + guildId + "_" + DateTime.Now.Ticks;
            Stopwatch timer = bot.Timing.StartTiming(basename);
            LegacyServer server = bot.DataStore.GetServer(guildId).Result;
            List<LegacyUser> susers = new List<LegacyUser>();
            susers = server.EnumUsers().Where(x => x != null && x.ContentFilterData.Total >= 1).ToList();
            susers.Sort(delegate (LegacyUser y, LegacyUser x)
            {
                int xdiff = x.ContentFilterData.Total.CompareTo(y.ContentFilterData.Total);
                if (xdiff != 0)
                {
                    return xdiff;
                }
                else
                {
                    return x.Xp.CompareTo(y.Xp);
                }
            });

            string scoreboard = @"";

            foreach (LegacyUser user in susers)
            {
                scoreboard += GetUserCard(user);
            }

            bot.Timing.StopTiming(timer);
            return scoreboard;
        }

        private static readonly string UserTemplate = $@"
        <user>
            <img id='avatar' loading='lazy' async accessKey='$ID' src='$PICURL'>
            <name>$NAME<discrim>#$DISCRIM</discrim></name><br>
            <level>Level: $LEVEL</level><br>
            <level>Warnings: $WARNCOUNT</level><br>
            <desc>Emotes: $EMOTXT</desc>
            <div class='pb'>
                <span style='width: $EMOP%;'></span>
            </div>
            <desc>Invites: $INVTXT</desc>
            <div class='pb'>
                <span style='width: $INVP%;'></span>
            </div>
            <desc>Mentions $TAGTXT</desc>
            <div class='pb'>
                <span style='width: $TAGP%;'></span>
            </div>
            <desc>Bad Language: $BLTXT</desc>
            <div class='pb'>
                <span style='width: $BLP%;'></span>
            </div>
        </user>";
        private Bot bot;

        public BehaviorBoardGenerator(Bot bot)
        {
            this.bot = bot;
        }

        public string GetUserCard(LegacyUser user)
        {
            if (user.Server == null) user.Server = bot.DataStore.GetServer(user.ServerId).Result;
            string a = UserTemplate
                .Replace("$PICURL", user.Left ? "https://thearcanebrony.net/bots/placeholder.png" : user.AvatarUrl)
                .Replace("$NAME", user.Username + "")
                .Replace("$DISCRIM", user.Discriminator)
                .Replace("$LEVEL", user.XpLevel + "")
                .Replace("$ID", user.UserId + "")
                .Replace("$WARNCOUNT", Math.Ceiling(user.ContentFilterData.Total) + "")
                .Replace("$EMOTXT", $"{Math.Round(user.ContentFilterData.Emotes, 2, MidpointRounding.ToPositiveInfinity)}/{user.Server.ContentFilter.EmoteTreshold}")
                .Replace("$INVTXT", $"{Math.Round(user.ContentFilterData.Invites, 2, MidpointRounding.ToPositiveInfinity)}/{user.Server.ContentFilter.InviteTreshold}")
                .Replace("$TAGTXT", $"{Math.Round(user.ContentFilterData.Mentions, 2, MidpointRounding.ToPositiveInfinity)}/{user.Server.ContentFilter.MentionTreshold}")
                .Replace("$BLTXT", $"{Math.Round(user.ContentFilterData.BadLang, 2, MidpointRounding.ToPositiveInfinity)}/{user.Server.ContentFilter.BadLangTreshold}")
                .Replace("$EMOP", Math.Min(100, Math.Round(user.ContentFilterData.Emotes / user.Server.ContentFilter.EmoteTreshold * 100, 1)) + "")
                .Replace("$INVP", Math.Min(100, Math.Round(user.ContentFilterData.Invites / user.Server.ContentFilter.InviteTreshold * 100, 1)) + "")
                .Replace("$TAGP", Math.Min(100, Math.Round(user.ContentFilterData.Mentions / user.Server.ContentFilter.MentionTreshold * 100, 1)) + "")
                .Replace("$BLP", Math.Min(100, Math.Round(user.ContentFilterData.BadLang / user.Server.ContentFilter.BadLangTreshold * 100, 1)) + "");
            return a;
        }
    }
}
