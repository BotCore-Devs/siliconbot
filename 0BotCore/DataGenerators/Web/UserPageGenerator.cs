﻿using CoreBot.Structures.DataStorage;

using System;
using System.Web;

namespace BotCore.DataGenerators.Web
{
    public class UserPageGenerator
    {
        private static readonly string Html = @"<meta charset='UTF-8'><style>
.pb {
height: 25px;
    background-color: black;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
border-radius: 25px;
}
.pb > span {
    display: block;
    height: 100%;
    border-bottom-right-radius: 25px;
    border-bottom-left-radius: 25px;
border-radius: 25px;
    background-color: blue;
    position: relative;
}
body {background-color:#242424; color:#ffffff; font-family:'Segoe UI';overflow:hidden;}
rank {font-size:67.5px; color:#7d26cd; line-height:48px; vertical-align:middle;}
xp {font-size:12; vertical-align:middle; color:#a2a2a2}
name {font-size:20; vertical-align:middle;}
level {font-size:20; vertical-align:middle;}
discrim {font-size:10; vertical-align:middle}
user {width:100%; display:inline-block; background-color:#a2a2a20f; border-radius:25px; padding-top:20px; margin:5px;}
#avatar {height:64px; border-radius: 50%; vertical-align: middle; }
</style>
<meta charset='UTF-8'>
<center>
    <div id='users'>";
        private static readonly string UserTemplate = $@"
        <user>
            <img id='avatar' loading='lazy' src='$PICURL'>
            <name>$NAME<discrim>#$DISCRIM</discrim></name><br>
            <level>$LEVEL</level>
            <xp>($XPC/$XPG XP)</xp><br>
            <div class='pb'>
                <span style='width: $XPP%'></span>
            </div>
        </user>";

        public static string Generate(ulong guildId, LegacyUser user)
        {
            string userPanel = UserTemplate.Replace("$PICURL", user.AvatarUrl)
                .Replace("$NAME", user.Username + "")
                .Replace("$DISCRIM", user.Discriminator)
                .Replace("$LEVEL", user.XpLevel + "")
                .Replace("$XPC", user.Xp + "")
                .Replace("$XPG", user.XpGoal + "")
                .Replace("$XPP", Math.Round((double)user.Xp / user.XpGoal * 100, 1) + "")
                .Replace("$ID", user.UserId + "");
            user.Quotes.Sort(delegate (QuotedMessage y, QuotedMessage x)
            {
                return x.Rating.CompareTo(y.Rating);
            });
            foreach (QuotedMessage quote in user.Quotes)
            {
                userPanel += $"            <a href='{quote.MessageUrl}'><p>{HttpUtility.HtmlEncode(quote.Text)}</p></a>";
            }

            userPanel += "        </user>";
            return Html + userPanel;
        }
    }
}
