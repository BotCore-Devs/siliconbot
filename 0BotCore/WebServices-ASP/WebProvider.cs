﻿using BotCore.Structures.Base;
using CoreBot;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace BotCore.WebServices_ASP
{
    internal class WebProvider
    {
        public static void Init(Bot bot)
        {
            BotHelper.BotWebInit = true;
            CreateHostBuilder(bot, new List<string>().ToArray()).Build().RunAsync();
        }

        public static bool PortFree(int port)
        {
            if (port == 87) return false;
            using TcpClient tcpClient = new TcpClient();
            try
            {
                Console.WriteLine("Testing port " + port);
                tcpClient.Connect("127.0.0.1", port);
                tcpClient.Close();
                tcpClient.Dispose();
                return false;
            }
            catch (Exception)
            {
                Console.WriteLine("Found free port " + port);
                return true;
            }
        }

        private static readonly bool RandomPort = false;

        public static IHostBuilder CreateHostBuilder(Bot bot, string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddDebug();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    int defaultPort = RandomPort ? 0 : BotManager.GetNextPort;
                    bot.Port = defaultPort;
                    webBuilder.UseStartup<Startup>().UseUrls(new[]
                        {"http://0.0.0.0:" + defaultPort, "http://thearcanebrony.net:" + defaultPort});
                });
        }
    }
}