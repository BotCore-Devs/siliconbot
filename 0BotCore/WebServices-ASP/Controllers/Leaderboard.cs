﻿using BotCore.DataGenerators.Web;
using BotCore.Structures.Base;
using BotCore.WebServices;

using CoreBot.Structures.DataStorage;

using DSharpPlus.Entities;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BotCore.WebServices_ASP.Controllers
{
    [Controller]
    [Route("/leaderboards")]
    public class LeaderboardListController : Controller
    {
        public LeaderboardListController(Bot bot)
        {
            this.bot = bot;
        }
        private Bot bot;
        [HttpGet]
        public ContentResult GetList()
        {
            string resp = NavbarProvider.GetNavBar() + @"<meta charset='UTF-8'><style>
a {text-decoration: none; color: white;}
body {background-color:#242424; color:#ffffff; font-family:'Segoe UI';}
name {font-size:20; vertical-align:middle;}
members {font-size:20; vertical-align:middle;}
server {width:500px; display:inline-block; background-color:#a2a2a20f; border-radius:25px; padding-top:20px; margin:5px;padding-bottom:20px;}
#icon {height:64px; border-radius: 50%; vertical-align: middle; }
</style>
<center>";
            foreach (KeyValuePair<ulong, DiscordGuild> g in bot.Client.Guilds.OrderByDescending(dg => dg.Value.MemberCount))
            {
                //resp += $"{g.Value.Name}</a><br>";
                resp += $@"
    <a href='/leaderboards/{g.Key}'>
        <server>
            <img id='icon' loading='lazy' src='{g.Value.IconUrl}'>
            <name>{g.Value.Name}</name><br>
            <level>{g.Value.MemberCount} members</level>
        </server>
    </a>";
            }
            return new ContentResult
            {
                ContentType = "text/html",
                Content = resp
            };
        }
        [HttpGet("{ServerID}")]
        public ContentResult GetBoard(ulong serverId)
        {
            string resp = NavbarProvider.GetNavBar();
            if (bot.Client == null)
            {
                resp += "Bot is still starting, please wait!";
            }
            else if (bot.Client.Guilds.ContainsKey(serverId))
            {
                resp += bot.LeaderboardGenerator.Generate(serverId);
            }
            else
            {
                resp += $"Server {serverId} not found!";
            }

            return new ContentResult
            {
                ContentType = "text/html",
                Content = resp
            };
        }
        [HttpGet("{ServerID}/{UserID}")]
        public async Task<ContentResult> GetUserBoard(ulong serverId, ulong userId)
        {
            string resp = NavbarProvider.GetNavBar();
            if (bot.Client == null)
            {
                resp += "Bot is still starting, please wait!";
            }
            else if (bot.Client.Guilds.ContainsKey(serverId))
            {
                resp = resp.Replace(NavbarProvider.GetNavBar(), "");
                LegacyUser user;
                if ((user = (await bot.DataStore.GetServer(serverId)).GetUserReadOnly(userId)) != null)
                {
                    resp += UserPageGenerator.Generate(serverId, user);
                }
                else
                {
                    resp += "User not found!";
                }
            }
            else
            {
                resp += "Server not found!";
            }

            return new ContentResult
            {
                ContentType = "text/html",
                Content = resp
            };

        }
        [HttpGet("{ServerID}/{UserID}/UpdateAvatar")]
        public async Task<ContentResult> GetAvatar(ulong serverId, ulong userId)
        {
            string resp = "";
            if (bot.Client == null)
            {
                resp += "Bot is still starting, please wait!";
            }
            else if (bot.Client.Guilds.ContainsKey(serverId))
            {
                LegacyUser user;
                if ((user = (await bot.DataStore.GetServer(serverId)).GetUserReadOnly(userId)) != null)
                {
                    try
                    {
                        string avatarUrl = (await bot.Client.GetUserAsync(userId)).AvatarUrl;
                        user.AvatarUrl = avatarUrl;
                        user.ServerId = serverId;
                        user.Save();
                        resp += $"<img id='avatar' loading='lazy' src='{avatarUrl}'>";
                    }
                    catch (Exception e)
                    {
                        resp += $"<b>!!!</b><b style='display: none;'>{e}</b>";
                    }
                }
                else
                {
                    resp += "User not found!";
                }
            }
            else
            {
                resp += "Server not found!";
            }

            return new ContentResult
            {
                ContentType = "text/html",
                Content = resp
            };

        }
    }
}
