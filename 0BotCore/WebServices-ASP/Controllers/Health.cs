﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;
using BotCore.Structures.Base;
using BotCore.WebServices;
using DSharpPlus.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BotCore.WebServices_ASP.Controllers
{
        [Controller]
        [Route("/health")]
        public class HealthController : ControllerBase
        {
            public HealthController(Bot bot)
            {
                this.bot = bot;
            }
            private Bot bot;

            [HttpGet]
            public async Task<ContentResult> Get()
            {
                if (System.DateTime.Now.Subtract(bot.LastUpdated).TotalSeconds > 30)
                {
                    bot.Totalusers = bot.Onlineusers = 0;
                    System.Console.WriteLine(System.DateTime.Now.Subtract(bot.LastUpdated).TotalSeconds);
                    bot.LastUpdated = System.DateTime.Now;
                    foreach (KeyValuePair<ulong, DiscordGuild> guild in bot.Client.Guilds)
                    {
                        bot.Totalusers += guild.Value.MemberCount;
                        bot.Onlineusers += (await guild.Value.GetAllMembersAsync()).Count(x => x.Presence != null && x.Presence.Status != UserStatus.Offline);
                    }
                }
                return new ContentResult { Content = JsonConvert.SerializeObject(HealthInfo.GetHealthInfo(), Formatting.Indented), ContentType = "application/json"};
            }
        }
        public class HealthInfo
        {
            public static Dictionary<string, HealthInfo> GetHealthInfo()
            {
                Dictionary<string, HealthInfo> healthInfos = new();
                foreach (var bot in BotManager.Bots)
                {
                    healthInfos.Add(bot.Key, new(bot.Value));
                }
                return healthInfos;
            }

            public HealthInfo(Bot bot)
            {
                if (bot == null)
                {
                    Latency = RecommendedShardCount = MaxShardCount = CurrentShardCount = -404;
                }
                else {
                    Latency = bot.Client.Ping;
                    WebPort = bot.Port;
                    if (GatewayConnected = bot.Client.GatewayInfo != null)
                    {
                        RecommendedShardCount = bot.Client.GatewayInfo.ShardCount;
                        MaxShardCount = bot.Client.GatewayInfo.SessionBucket.MaxConcurrency;
                        CurrentShardCount = bot.Client.ShardCount;
                    }
                }
            }

            public int Latency = -1;
            public bool GatewayConnected = false;
            public int CurrentShardCount = -1;
            public int RecommendedShardCount = -1;
            public int MaxShardCount = -1;
            public int WebPort = -1;
        }
}