﻿using BotCore.Structures.Base;
using BotCore.WebServices;

using DSharpPlus.Entities;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotCore.WebServices_ASP.Controllers
{
    [Controller]
    [Route("/")]
    public class HomepageController : ControllerBase
    {
        public HomepageController(Bot bot)
        {
            this.bot = bot;
        }
        private Bot bot;

        [HttpGet]
        public async Task<ContentResult> Get()
        {
            if (System.DateTime.Now.Subtract(bot.LastUpdated).TotalSeconds > 30)
            {
                bot.Totalusers = bot.Onlineusers = 0;
                System.Console.WriteLine(System.DateTime.Now.Subtract(bot.LastUpdated).TotalSeconds);
                bot.LastUpdated = System.DateTime.Now;
                foreach (KeyValuePair<ulong, DiscordGuild> guild in bot.Client.Guilds)
                {
                    bot.Totalusers += guild.Value.MemberCount;
                    bot.Onlineusers += (await guild.Value.GetAllMembersAsync()).Count(x => x.Presence != null && x.Presence.Status != UserStatus.Offline);
                }
            }

            return new ContentResult { Content = $@"{NavbarProvider.GetNavBar()}
<style>
  center{{ color: white; }}
  .avatar {{ border-radius:50%; height:10%; }}
  h1, h2, h3, h4, h5, h6, p {{font-family: 'Segoe UI';}}
  #footer {{
    position: fixed;
    bottom: 0;
    vertical-align: bottom;
    width: auto;
    height: 24px;
    border: none;
  }}
</style>
<div style='height: 60px;'></div>
<center>
  <img class=avatar src='{bot.Client.CurrentUser.GetAvatarUrl(DSharpPlus.ImageFormat.WebP, 2048)}' style='height:20%;'><br><br>
  <h1>{bot.Name}</h1><br>
  <p><i>{bot.Onlineusers}</i> of <i>{bot.Totalusers}</i> Members online in <i>{bot.Client.Guilds.Count}</i> servers</p>
</center>
<div id=footer>
  <div style='width:4px;'></div><img src='https://img.shields.io/badge/BotCore-{CoreBot.RuntimeInfo.Ver}-green'>
</div>", ContentType = "text/html" };
        }
    }
}
