﻿using BotCore.Structures.Base;

using DSharpPlus;
using DSharpPlus.EventArgs;

using System;
using System.Threading.Tasks;

namespace BotCore.EventHandlers
{
    public class GuildAvailableHandler
    {
        private Bot bot;

        public GuildAvailableHandler(Bot bot)
        {
            this.bot = bot;
        }

        public int AvailableGuilds { get; private set; }

        public async Task HandleGuildAvailable(DiscordClient client, GuildCreateEventArgs e)
        {
            Console.WriteLine($"Discovered guild {e.Guild.Name}");
            bot.ContentFilterWorker.LoadServer(e.Guild.Id);
            AvailableGuilds++;
            //sw = bot.timings.GotoNext (sw, $"Updating user pages for {e.Guild.Name}");
            //LegacyServer srv = await bot.dataStore.GetServer (e.Guild.Id);
            //srv.EnumUsers ();

            if (AvailableGuilds == client.Guilds.Count)
            {
                Console.WriteLine($"Fully logged in as {client.CurrentUser.Username}");
                //Client.EditCurrentUserAsync(avatar: File.Open("avatar.png", FileMode.Open));
                await Task.Delay(1);
            }
        }
    }
}
