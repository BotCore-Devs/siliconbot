﻿using System.Diagnostics;
using System.Linq;
using BotCore.Structures.Base;
using CoreBot;
using CoreBot.Structures.DataStorage;
using DSharpPlus.Entities;

namespace CoreBot.EventHandlers.MessageHandlerModules
{
    public class Xp
    {
        private Bot bot;
        private DiscordRole memberRole;

        public Xp(Bot bot)
        {
            this.bot = bot;
        }

        public async void CheckMessage(string timingId, LegacyServer server, LegacyUser author, DiscordMessage msg)
        {
            
            if(bot.Name == "Silicon Bot") if(memberRole == null) memberRole = bot.Client.GetGuildAsync(391478785754791949).Result.GetRole(468007605466300416);
            Stopwatch st = bot.Timing.StartTiming(timingId + ".XP");
            //XP system
            var xp = 0;
            var newxp = 0;
            string[] words = msg.Content.Split(' ');
            if (!msg.Author.IsBot && server.LevelsEnabled)
            {
                xp = RuntimeInfo.Rnd.Next(25, 75);
                newxp = (int)(xp * (msg.Content.Length > words.Length * 3 ? 1 : 0) * (words.Length < 75 ? words.Length / 10d : (100 - words.Length) / 50d));
                author.Xp += (int)(newxp * author.XpMultiplier);
                if (author.Xp >= author.XpGoal)
                {
                    while (author.Xp >= author.XpGoal)
                    {
                        author.Xp -= author.XpGoal; 
                        author.XpLevel += 1;  
                    }

                    if (bot.Name == "Silicon Bot" && msg.Member().Roles.Contains(memberRole))
                        await msg.Member().GrantRoleAsync(memberRole, "AutoRole: user leveled up");
                    if (server.LevelUpMessagesEnabled) try
                        {
                            await msg.Channel.SendMessageAsync($"Congratulations! {msg.Member().DisplayName()} just achieved level {author.XpLevel}! :partying_face:");
                        }
                        catch (DSharpPlus.Exceptions.UnauthorizedException)
                        {
                            try
                            {
                                await msg.CreateReactionAsync(DiscordEmoji.FromName(bot.Client, ":partying_face:"));
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                }
            }

            bot.Timing.StopTiming(st);
        }
    }
}