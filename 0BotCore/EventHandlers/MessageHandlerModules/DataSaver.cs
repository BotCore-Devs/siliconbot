﻿using System;
using System.Diagnostics;
using BotCore.Structures.Base;
using BotCore.Structures.DbInterface;
using CoreBot.Structures.DataStorage;
using DSharpPlus.Entities;

namespace CoreBot.EventHandlers.MessageHandlerModules
{
    public class DataSaver
    {
        private Bot bot;
        public DataSaver(Bot _bot)
        {
            bot = _bot;
        }
        public async void Save(string timingId, LegacyServer server, LegacyUser author)
        {
            if (bot == null)
            {
                Console.WriteLine("Bot is null! Not saving!\b");
            }
            else
            {
                Stopwatch st = bot.Timing.StartTiming(timingId + ".Save");
                author.Save();
                server.Save();
                bot.Timing.StopTiming(st);
            }
        }
        public async void Migrate(string timingId, LegacyServer server, LegacyUser author)
        {
            Stopwatch st = bot.Timing.StartTiming(timingId + ".Migrate");
            try
            {
                author.MigrateToDb();
            }
            catch (Exception er)
            {
                Console.WriteLine("Something has gone wrong migrating to DB!");
                Console.WriteLine(er);
            }

            bot.Timing.StopTiming(st);
        }
    }
}