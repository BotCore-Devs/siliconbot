﻿using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using BotCore.Structures.Base;
using CoreBot;
using CoreBot.Structures.DataStorage;
using DSharpPlus;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace CoreBot.EventHandlers.MessageHandlerModules
{
    public class Censorship
    {
        private Bot bot;
        public Censorship(Bot bot)
        {
            this.bot = bot;
        }

        public async void CheckMessage(string timingId, LegacyServer server, LegacyUser author, DiscordMessage msg)
        {
            Stopwatch sw = bot.Timing.StartTiming(timingId + ".censorship");
            // if (Regex.Matches(e.Message.Content.RemoveDiacritics().ToLower(), @"(how)(\s)+(\S+)(\s)+(is)(\s)+(our|your)(\s)+(\S+)").Count >= 1 && e.Guild.Id == 361634042317111296) e.Channel.SendMessageAsync(e.Message.Author.Mention + ", no... Stop it... Get some help...");
//#if !OMNIBOT
                if (!author.IsStaff && msg.Content.ContainsAnyOf(server.WordFilter))
                {
                    Stopwatch st = bot.Timing.StartTiming(timingId + ".Filter");
                    author.BlockedWordCount++;
                    await msg.DeleteAsync();
                    await msg.Channel.SendMessageAsync("That word is not allowed here!");
                    if (author.BlockedWordCount % 5 == 0)
                    {
                        //await e.Channel.SendMessageAsync("Muted for 30 seconds!");
                        //author.Mute(30);
                    }

                    bot.Timing.StopTiming(st);
                }
//#endif
                if (server.ContentFilter.Enabled && msg.Author.Id != bot.Client.CurrentUser.Id)
                {
                    Stopwatch st = bot.Timing.StartTiming(timingId + ".ContentFilter");

                    LegacyContentFilter hm = server.ContentFilter;
                    LegacyContentFilterData hd = author.ContentFilterData;
                    if (hm.MentionsEnabled)
                    {
                        hd.Mentions += msg.MentionedUsers.Count + msg.MentionedRoles.Count;
                    }
                    if (hm.BadLangEnabled)
                    {
                        hd.BadLang += msg.Content.CountInstancesOfAll(server.ContentFilter.BadStrings);
                    }
                    if (hm.InvitesEnabled)
                    {
                        hd.Invites += msg.Content.CountInstances("discord.gg");
                    }
                    if (hm.EmotesEnabled)
                    {
                        hd.Emotes += Regex.Matches(msg.Content, @"(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])").Count + msg.Content.CountInstances("<:");
                    }
                    if (hd.Total > 0)
                    {
                        bot.ContentFilterWorker.StartUser(msg.Channel.Guild.Id, msg.Author.Id);
                    }

                    if (hm.MentionsEnabled && hd.Mentions >= hm.MentionTreshold
                        || hm.BadLangEnabled && hd.BadLang >= hm.BadLangTreshold
                        || hm.InvitesEnabled && hd.Invites >= hm.InviteTreshold
                        || hm.EmotesEnabled && hd.Emotes >= hm.EmoteTreshold)
                    {
                        await msg.Channel.AddOverwriteAsync(msg.Member(), deny: Permissions.All);
                        new Thread(async () => { while (hd.Total != 0) { Thread.Sleep(1000); } await msg.Channel.PermissionOverwrites.Where((user) => user.Id == msg.Author.Id).ToArray()[0].DeleteAsync(); }).Start();
                        await msg.Channel.SendMessageAsync($"Muted\n" + "```json\n" + JsonConvert.SerializeObject(author.ContentFilterData, Formatting.Indented) + "```");
                    }

                    if (msg.Channel.Guild.Id == 643482410339794974 && !msg.Author.IsBot)
                    {
                        await msg.Channel.SendMessageAsync("```json\n" + JsonConvert.SerializeObject(author.ContentFilterData, Formatting.Indented) + "```");
                    }
                    bot.Timing.StopTiming(st);
                }



            bot.Timing.StopTiming(sw);
        }
    }
}