﻿using BotCore.Structures.Base;
using BotCore.Structures.DbInterface;

using CoreBot.EventHandlers.Commands;
using CoreBot.Structures.DataStorage;

using DSharpPlus;

using DSharpPlus.EventArgs;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CoreBot.EventHandlers.MessageHandlerModules;

namespace CoreBot.EventHandlers
{
    public class MessageHandler
    {
        private static int _i;
        private Bot bot;

        private Censorship censorship;
        private Xp xp;
        private MessageInteractions messageInteractions;
        private DataSaver dataSaver;

        public MessageHandler(Bot bot)
        {
            this.bot = bot;
            censorship = new(bot);
            xp = new(bot);
            messageInteractions = new(bot);
            dataSaver = new(bot);
        }

        public async Task HandleMessage(DiscordClient client, MessageCreateEventArgs e)
        {
            //vars
            try
            {
                string timingId = $"Message{_i++}";
                Stopwatch mst = bot.Timing.StartTiming(timingId);
                Stopwatch st = bot.Timing.StartTiming(timingId + ".LoadData");
                Stopwatch stl = bot.Timing.StartTiming(timingId + ".LoadGuild");
                bool isDm = e.Channel.IsPrivate;
                LegacyServer server = await bot.DataStore.GetServer(isDm ? 0 : e.Guild.Id);
                stl = bot.Timing.GotoNext(stl, $"{timingId}.LoadUser");
                LegacyUser author, pinged;
                try
                {
                    author = server.GetUser(e.Author.Id, isDm);
                    pinged = server.GetUser(e.Author.Id, isDm);
                    author.Server = server;
                }
                catch
                {
                    bot.Timing.StopTiming(stl);
                    bot.Timing.StopTiming(st);
                    bot.Timing.StopTiming(mst);
                    Console.WriteLine($"Error handling message at {e.Message.JumpLink}\nReason: Failed to load user/server data!");
                    await e.Channel.SendMessageAsync(
                        $"Error handling message at {e.Message.JumpLink}\nReason: Failed to load user/server data!");
                    return;
                }
                st = bot.Timing.GotoNext(st, timingId + ".UpdateDB");
                server.ContentFilter.BadStrings = server.ContentFilter.BadStrings.Distinct().ToList();
                author.MessageCount++;
                author.UserId = e.Author.Id;
                server.GuildId = isDm ? 0 : e.Guild.Id;

                st = bot.Timing.GotoNext(st, timingId + ".UChecks");
                author.Username = e.Author.Username;
                author.Discriminator = e.Author.Discriminator;
                author.AvatarUrl = e.Author.AvatarUrl;

                if (!isDm && !author.Nicknames.Contains(e.Member()?.Nickname) && e.Member()?.Nickname != null)
                {
                    author.Nicknames.Add(e.Member()?.Nickname);
                }

                bot.Timing.StopTiming(stl);
                if (e.MentionedUsers.Count != 0 && e.MentionedUsers[0] != null)
                {
                    pinged = server.GetUser(e.MentionedUsers[0].Id, isDm);
                }
#if !SMALL
                censorship.CheckMessage(timingId, server, author, e.Message);
                xp.CheckMessage(timingId, server, author, e.Message);
                messageInteractions.CheckMessage(timingId, server, author, e.Message);
#endif
                if (e.Message.Content.StartsWith(server.Prefix) && !server.IgnoredChannels.Contains(e.Channel.Id))
                {
                    st = bot.Timing.GotoNext(st, timingId + ".CommandHandling");
                    await bot.CommandHandler.HandleCommandAsync(new CommandEnvironment(client, e, server, author, pinged));
                }
                if (bot == null)
                {
                    Console.WriteLine("Bot is null! Not saving!\b");
                }
                dataSaver.Save(timingId, server, author);
                //dataSaver.Migrate(timingId, server, author);
                Util.PrintSplit(" | ", BotManager.IsSingleBot ? timingId : bot.Name, e.Message.Timestamp.DateTime, mst.ElapsedMilliseconds + " ms", /*e.Guild != null ? e.Guild.Name.Substring(0, 11) : "DM",*/ (isDm ? "DM" : e.Channel.Name).PadRight(11), e.Author.Username.PadRight(16));
                Console.WriteLine(e.Message.Content);

                bot.Timing.StopTiming(st);
                bot.Timing.StopTiming(timingId);
            }
            catch (Exception err)
            {
                Console.WriteLine($"An error has occurred whilst handling a message:\n{err.Message}\n\nStacktrace:\n{err.StackTrace}");
            }
        }
    }
}
