﻿using System;
using System.Collections.Generic;
using System.Threading;
using BotCore.Structures.Base;
using Newtonsoft.Json;

namespace Omnibot.EventHandlers
{
    public class TimedEventManager
    {
        private Bot bot;
        public TimedEventManager(Bot _bot)
        {
            bot = _bot;
        }

        public Dictionary<string, TimedEvent> TimedEvents = new();

        /// <summary>
        /// Start a timed event
        /// </summary>
        /// <param name="code">Code to run</param>
        /// <param name="name">Name for the timed event</param>
        /// <param name="time">Time to run this code at</param>
        /// <param name="delay"></param>
        public TimedEvent StartTimedEvent(Action code, string name, TimeSpan time, TimeSpan? delay = null)
        {
            TimedEvent timedEvent = new TimedEvent()
                {RunTime = time, Interval = delay ?? Timeout.InfiniteTimeSpan, CodeToInvoke = code};
            TimedEvents.Add(name, timedEvent);
            return timedEvent;
        }
        public void CancelTimedEvent(string name)
        {
            if (TimedEvents.ContainsKey(name))
            {
                TimedEvents[name].Cancel();
            }
        }
    }

    public class TimedEvent
    {
        [JsonIgnore] public TimeSpan TimeUntilInvocation => RunTime - DateTime.Now.TimeOfDay;
        [JsonIgnore] public Timer Timer { get; set; }
        private TimeSpan interval = Timeout.InfiniteTimeSpan;
        public TimeSpan Interval
        {
            get => interval;
            set
            {
                interval = value;
                Timer?.Change(TimeUntilInvocation, interval);
            }
        }
        public TimeSpan RunTime { get; set; }
        public Action CodeToInvoke { get; set; }
        public TimedEvent Start()
        {
            if (RunTime < DateTime.Now.TimeOfDay) RunTime = RunTime.Add(TimeSpan.FromDays(1));
            Timer = new Timer(x =>
            {
                CodeToInvoke.Invoke();
            }, null, TimeUntilInvocation, interval);
            return this;
        }
        public void RunNow()
        {
            Cancel();
            CodeToInvoke.Invoke();
        }
        public void Cancel()
        {
            Timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
            Timer.Dispose();
        }
    }
}