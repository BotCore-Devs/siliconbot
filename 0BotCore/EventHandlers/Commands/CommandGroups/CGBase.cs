﻿/*
 *  This code was last modified by ThatOneScreensaver
 *
 *  Whenever you're about to commit/push this code, please try to update this
 *
 */


using BotCore.Structures.Base;

using CoreBot.Structures.Command;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgBase
    {
        public static void SetupCommands(Bot bot)
        {
            /* Ping Command */
            bot.CommandHandler.RegisterCommand("ping",
                // Command Statement in the help menu
                new CommandDefinition(CommandCategory.Base, "Bot latency", async (ce) =>
                {
                    /* Send Message in Channel as follows "Latency: [insert bot ping here]" */
                    await ce.SendMessageAsync($"Latency: {ce.Client.Ping} ms");

                })
            );
            /* End Of Command Code */

            /* Debug Info Command */
            bot.CommandHandler.RegisterCommand("debuginfo",
                // Command Statement in the help menu
                new CommandDefinition(CommandCategory.Base, "Enables debug info in chat", async (ce) =>
                {
                    ce.Pinged.DebugInfo = !ce.Pinged.DebugInfo;
                    await ce.SendMessageAsync($"Debug info has been {(ce.Pinged.DebugInfo ? "en" : "dis")}abled for user {(ce.E.MentionedUsers.Count > 0 ? ce.E.Guild.Members[ce.E.MentionedUsers[0].Id].DisplayName() : ce.E.Member().DisplayName())}!");
                }));
            /* End Of Command Code */

            /* Bot Info Command */
            bot.CommandHandler.RegisterCommand("botinfo",
                // Command Statement in the help menu
                new CommandDefinition(CommandCategory.Base, "Information about the bot", async (ce) =>
                {
                    try
                    {
                        // Initial Integer Values (???)
                        int servercount = 0, channelcount = 0, usercount = 0, humancount = 0, botcount = 0, trackedUsers = 0; // bot.dataStore.Config.TrackedUserCount;
                        foreach (KeyValuePair<ulong, DiscordGuild> guild in ce.Client.Guilds)
                        {
                            servercount++;
                            usercount += guild.Value.MemberCount;
                            channelcount += guild.Value.Channels.Count;
                            /* Please, for the love of god and in order for this to work, MAKE SURE TO HAVE SERVER MEMBERS INTENT ENABLED IN THE BOT SETTINGS MENU!!! */
                            IReadOnlyCollection<DiscordMember> members;
                            try
                            {
                                members = await guild.Value.GetAllMembersAsync();
                                foreach (DiscordMember member in members)
                                {
                                    if (member.IsBot)
                                    {
                                        botcount += 1;
                                    }
                                    else
                                    {
                                        humancount += 1;
                                    }
                                }
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                        await ce.SendMessageAsync($"Servers: {servercount}\n" +
                                                            $"Total channels: {channelcount}\n" +
                                                            $"Total users: {(usercount == 0 ? usercount : "Bot doesn't have the members intent enabled!")}\n" +
                                                            $"  - Users: {(humancount == 0 ? humancount : "Bot doesn't have the members intent enabled!")}\n" +
                                                            $"  - Bots: {(botcount == 0 ? botcount : "Bot doesn't have the members intent enabled!")}\n" +
                                                            $"Users in database: {trackedUsers}\n" +
                                                            $"Commands: {bot.CommandHandler.Commands.Count}\n" +
                                                            $"Uptime: {Process.GetCurrentProcess().StartTime.Subtract(DateTime.Now).Duration()}\n\n" +
                                                            // Lovely bot devs. For those new programmers coming into the project, feel free to add your self here as a bot dev!
                                                            $"**Bot created by:**" +
                                                            $"\n- The Arcane Brony#9669" +
                                                            $"\n- A Screensaver#0980" +
                                                            $"\n- gsfordham#8222");
                    }

                    /* Yeah so your command didn't run so lets just return an exception */
                    catch (Exception ee)
                    {
                        /* Send a message basically saying that something has gone wrong while running your command and then it shows the exception */
                        await ce.SendMessageAsync("Something has gone wrong processing this command: ```\n" + ee + "```");
                    }

                })
            );
            /* End Of Command Code */

            /* User Count Command */
            bot.CommandHandler.RegisterCommand("usercount",
                // Command Statement in the help menu
                new CommandDefinition(CommandCategory.Base, "Amount of users and bots", async (ce) =>
                {
                    int users = 0, bots = 0, online = 0;
                    IReadOnlyCollection<DiscordMember> members = null;
                    try
                    {
                        // Get the user count of all the members in the server from which of course, this command is executed from.
                        members = await ce.E.Guild.GetAllMembersAsync();
                    }

                    // Server Members Intent is disabled
                    catch
                    {
                        // Send message stating that this won't work unless members intent gets turned back on
                        await ce.SendMessageAsync("Members intent not enabled on this bot! Please report this!");
                    }

                    if (members != null)
                        foreach (DiscordMember user in members)
                        {
                            // Self-explanatory
                            if (user.IsBot)
                            {
                                bots++;
                            }

                            // User *isn't* a bot
                            else
                            {
                                users++;
                                if (user.Presence != null && !(user.Presence.Status == UserStatus.Offline ||
                                                               user.Presence.Status == UserStatus.Invisible))
                                {
                                    online++;
                                }
                            }
                        }

                    // Final End Result of Command Execution
                    await ce.SendMessageAsync(
                        $"```moonscript\n" +
                        $"Member count\n" +
                        $"------------\n" +
                        $" Users: {users}\n" +
                        $"Online: {online}\n" +
                        $"  Bots: {bots}\n" +
                        $"+-----------\n" +
                        $" Total: {users + bots}```");
                })
            );
            /* End Of Command Code */

            /* Cleanup Command */
            bot.CommandHandler.RegisterCommand("cleanup",
                new CommandDefinition(CommandCategory.Base)
                {
                    /* Category in which the Command will be located in */
                    Category = CommandCategory.Base,
                    /* Description stating what the command does */
                    Description = $"Clean up {bot.Name} spam",
                    Action = async (ce) =>
                    {
                        int c = 0;
                        List<DiscordMessage> msgs = (await ce.E.Channel.GetMessagesAsync(500)).ToList().FindAll(message => message.Author.Id == ce.Client.CurrentUser.Id || message.Content.StartsWith(ce.Server.Prefix));
                        for (int i = 0; i < msgs.Count; i++)
                        {
                            if (ce.E.Guild.Members.TryGetValue(ce.Client.CurrentUser.Id, out DiscordMember member) && ce.E.Channel.PermissionsFor(member).HasPermission(Permissions.ManageMessages) || msgs[i].Author.Id == ce.Client.CurrentUser.Id)
                                try
                                {
                                    await msgs[i].DeleteAsync();
                                    c++;
                                    Console.WriteLine($"{msgs[i].CreationTimestamp}");
                                }

                                catch (UnauthorizedException)// Yeah Idek what triggers this or what but nevertheless it is here, present
                                {
                                    await ce.SendMessageAsync($"Unknown Unauthorized Exception, please report this to `The Arcane Brony#9669`, `A Screensaver#0980` or `gsfordharm#8222` as soon as possible. Thanks in advance!");
                                }


                            if (i == msgs.Count - 1)
                            {
                                msgs = (await ce.E.Channel.GetMessagesBeforeAsync(msgs[i].Id, 500)).ToList().FindAll(message => { return message.Author.Id == ce.Client.CurrentUser.Id || message.Content.StartsWith(ce.Server.Prefix); });
                                i = 0;
                            }

                            Console.Title = $"Deleting messages | Position in array: {i}/{msgs.Count} | Total deleted: {c}";
                        }
                        DiscordMessage msg = await ce.SendMessageAsync($"Deleted {c} messages!");
                        new Thread(() => { Thread.Sleep(5000); msg.DeleteAsync(); }).Start();
                    }
                }
            );
            /* End Of Command Code */

            /* User Info Command */
            bot.CommandHandler.RegisterCommand("userinfo",
                // Command Statement in the help menu
                new CommandDefinition(CommandCategory.Base, "Info about a user")
                {
                    Action = async (ce) =>
                    {
                        List<DiscordMember> members = new List<DiscordMember>();

                        try
                        {
                            members = (await ce.E.Guild.GetAllMembersAsync()).ToList();
                            members.Sort(delegate (DiscordMember x, DiscordMember y)
                            {
                                int xdiff = x.JoinedAt.CompareTo(y.JoinedAt);
                                return xdiff;
                            });
                        }

                        catch
                        {
                            // ignored
                        }

                        DiscordMember dmember = ce.E.Member();

                        if (ce.E.MentionedUsers.Count > 0)
                        {
                            dmember = ce.E.Guild.Members[ce.E.MentionedUsers[0].Id];
                        }

                        string info = "";
                        info += $" - Created at: {dmember.CreationTimestamp.DateTime}\n";
                        info += $" - Joined at: {dmember.JoinedAt}\n";

                        if (bot.Name != "Omnibot") try
                            {
                                info += $" - Game: {dmember.Presence.Activity.Name}\n";
                            }

                            catch
                            {
                                info += " - Game: failed to get game info.\n";
                            }

                        info += $" - ID: {dmember.Id}\n";
                        info += $" - Join pos: {members.IndexOf(dmember)}/{ce.E.Guild.MemberCount} ({ce.E.Guild.MemberCount - members.IndexOf(dmember)} after)\n";

                        if (bot.Name != "Omnibot") info += $" - Recorded message count: {ce.Pinged.MessageCount}\n";
                        if (bot.Name != "Omnibot") info += $" - Warn count: {ce.Pinged.Warnings.Count}\n";

                        if (ce.Pinged.Nicknames.Count > 0)
                        {
                            info += $" - Nickname history: ``` - {string.Join("\n - ", ce.Pinged.Nicknames)}```\n";
                        }

                        if (bot.Name != "Omnibot") info += $" - Invites used: ``` - {string.Join("\n -", ce.Pinged.InvitesUsed)}```\n";
                        if (bot.Name != "Omnibot") info += $" - Credits: {ce.Pinged.Credits}\n";

                        // Self-explanatory
                        if (ce.Pinged.IsStaff)
                        {
                            info += $" - Is staff\n";
                        }

                        // Self-explanatory
                        if (bot.Name != "Omnibot") if (ce.Pinged.Muted)
                        {
                            info += $" - Muted: {ce.Pinged.Muted} (Until: {ce.Pinged.UnmuteBy}, {(ce.Pinged.UnmuteBy - DateTime.Now).TotalSeconds} seconds remaining)\n";
                        }

                        // Send end result
                        await ce.SendMessageAsync($"User info for {dmember}:\n" + info);
                    }
                }
            );
            /* End Of Command Code */

            /* Server Info Command */
            bot.CommandHandler.RegisterCommand("serverinfo",
                new CommandDefinition(CommandCategory.Base, "Info about the server")
                {
                    Action = async (ce) =>
                    {
                        try
                        {
                            Console.WriteLine("Serverinfo trigger start");
                            string serverinfo = $"Server info for {ce.E.Guild}:\n";
                            serverinfo += $" - Member count: {ce.E.Guild.MemberCount} (DB: {ce.Server.Users.Count})\n";
                            serverinfo += $" - Channel count: {ce.E.Guild.Channels.Count}\n";
                            serverinfo += $" - Icon: <{ce.E.Guild.IconUrl}>\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Banner: <https://cdn.discordapp.com/banners/{ce.E.Guild.Id}/{ce.E.Guild.Banner}.png?size=2048>\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Splash: <{ce.E.Guild.SplashUrl}>\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - AFK Channel: {ce.E.Guild.AfkChannel} after {ce.E.Guild.AfkTimeout} seconds\n";
                            serverinfo += $" - Created at: {ce.E.Guild.CreationTimestamp}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Default message notification settings: {ce.E.Guild.DefaultMessageNotifications}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Description: {ce.E.Guild.Description}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Embed enabled: {ce.E.Guild.EmbedEnabled}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Emote count: {ce.E.Guild.Emojis.Count}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Explicit content filter: {ce.E.Guild.ExplicitContentFilter}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Features: {string.Join(", ", ce.E.Guild.Features)}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Large: {ce.E.Guild.IsLarge}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - MFA level: {ce.E.Guild.MfaLevel}\n";
                            serverinfo += $" - Owner: {ce.E.Guild.Owner}\n";
                            serverinfo += $" - Boost count: {ce.E.Guild.PremiumSubscriptionCount}\n";
                            serverinfo += $" - Boost tier: {ce.E.Guild.PremiumTier}\n";
                            serverinfo += $" - Role count: {ce.E.Guild.Roles.Count}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - System channel: {ce.E.Guild.SystemChannel}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Vanity URL code: {ce.E.Guild.VanityUrlCode}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Verification level: {ce.E.Guild.VerificationLevel}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Voice region: {ce.E.Guild.VoiceRegion}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Lockdown: {ce.Server.Lockdown}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Message count: {ce.Server.MessageCount}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Reset on leave: {ce.Server.ResetOnLeave}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Muted user count: {ce.Server.MutedUsers.Count}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Autorole on join: {ce.Server.AutoRole}\n";
                            if (bot.Name != "Omnibot") serverinfo += $" - Moderation enabled: {ce.Server.ModerationEnabled}";
                            await ce.SendMessageAsync(serverinfo);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }
                    }
                }
            );
            /* End Of Command Code */

            /* Donate Command */
            bot.CommandHandler.RegisterCommand("donate",
            new CommandDefinition(CommandCategory.Base, "Info about donating, list of donators")
            {
                Action = async (ce) =>
                {
                    await ce.SendMessageAsync($"You can donate at <https://paypal.me/TheArcaneBrony> or <https://patreon.com/TheArcaneBrony>.\n\nTop donators:\n\nKinoshita Shimizu: >500 EUR (hardware)\nCHRONOBODI: 55 EUR (PSU)");
                }
            });

            // Premium Command
            // Function: Gives a user info about <botcore> premium
            if (bot.Name != "Omnibot") bot.CommandHandler.RegisterCommand("premium",
                new CommandDefinition(CommandCategory.Base, "Info about premium membership")
                {
                    Action = async (ce) =>
                    {
                        await ce.SendMessageAsync($"You can donate at <https://paypal.me/TheArcaneBrony> or <https://patreon.com/TheArcaneBrony>.\n\nPremium will offer you more features and credits!\nPricing: 5$/month\nYou can pay a multiple of this price to add more time!\nMake sure to include your *__Discord user ID__* in the description so I can activate your features!");
                    }
                }
            );
        }
    }
}
/* End Of Code */
