﻿using BotCore.Structures.Base;

using CoreBot.Structures.Command;
using CoreBot.Structures.DataStorage;

using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgOwner
    {
        public static void SetupCommands(Bot bot)
        {
            bot.CommandHandler.RegisterCommand("run",
                new CommandDefinition(CommandCategory.Owner, "Run a payload")
                {
                    Action = async (ce) =>
                    {
                        Console.WriteLine(string.Join(", ", ce.Args));
                        switch (ce.Args[0])
                        {
                            case "giveallmember":
                                foreach (DiscordMember mmb in await ce.E.Guild.GetAllMembersAsync())
                                {
                                    if (!mmb.IsBot)
                                    {
                                        await mmb.GrantRoleAsync(ce.E.Guild.GetRole(ce.Server.Roles.Member));
                                    }
                                }

                                break;
                            case "testing":
                                await ce.SendMessageAsync($@"
Have you ever felt like {bot.Name} was unstable?
Have you ever wanted to see a little behind the scenes?
Have you ever wanted to help test the bot?
Have you ever wanted to suggest your own features?

Take this invite link straight into the bot development server!

https://discord.gg/VwKMryw");
                                break;
                            case "invites":
                                foreach (DiscordGuild guild in ce.Client.Guilds.Values)
                                {
                                    try
                                    {
                                        await ce.SendMessageAsync("https://discord.gg/" + (await guild.Channels.Values.First().CreateInviteAsync(max_uses: 1, temporary: true)).Code);
                                        Thread.Sleep(2000);
                                    }
                                    catch { await ce.SendMessageAsync($"Failed to create invite for {guild.Name}"); }
                                }
                                break;
                            case "10248647":
                                if (Dns.GetHostName() == "DESKTOP-0ABLLGL")
                                {
                                    Random rnd = new Random();
                                    foreach (DiscordMember member in ce.E.Guild.Members.Values)
                                    {
                                        LegacyUser user = (await bot.DataStore.GetServer(ce.E.Guild.Id)).GetUser(member.Id, false);
                                        user.XpLevel = rnd.Next(40);
                                        user.Xp = rnd.Next(user.XpGoal);
                                        user.AvatarUrl = member.AvatarUrl;
                                        user.Changed = true;
                                        user.Credits = (ulong)rnd.Next();
                                        user.Left = rnd.Next(0, 2) == 1;
                                        user.MessageCount = rnd.Next();
                                        user.Username = member.Username;
                                        user.Discriminator = member.Discriminator;

                                        user.Save();
                                    }
                                }
                                break;
                            case "getprunecount":
                                await ce.SendMessageAsync($"Pruneable members:\n  - 07 days inactivity: {await ce.E.Guild.GetPruneCountAsync()}\n  - 30 days inactivity: {await ce.E.Guild.GetPruneCountAsync(30)}");

                                break;
                            default:
                                await ce.SendMessageAsync("Payload not found!");
                                break;
                        }
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("cmd",
                new CommandDefinition(CommandCategory.Owner, "Run a console command")
                {
                    Action = (ce) =>
                    {
                        string cmd = "\"";
                        foreach (string arg in ce.Args)
                        {
                            cmd += arg + " ";
                        }
                        cmd = cmd.Trim();
                        cmd += "\"";

                        //await ce.SendMessageAsync($"Command being executed: `{cmd}`");
                        ProcessStartInfo psi = new ProcessStartInfo() { CreateNoWindow = false, UseShellExecute = false, FileName = BotManager.IsLinux ? "/bin/sh" : "cmd.exe", Arguments = (BotManager.IsLinux ? "-c " : "/c ") + cmd, RedirectStandardOutput = true, RedirectStandardError = true, RedirectStandardInput = true, };
                        new Thread(async () =>
                        {
                            Process p = Process.Start(psi);
                            await p?.WaitForExitAsync();
                            string stdout = Encoding.UTF8.GetString(p.StandardOutput.CurrentEncoding.GetBytes(await p.StandardOutput.ReadToEndAsync())),
                                stderr = Encoding.UTF8.GetString(p.StandardError.CurrentEncoding.GetBytes(await p.StandardError.ReadToEndAsync()));
                            if (stderr != "")
                            {
                                stdout += "STDERR:\n" + stderr;
                            }

                            List<string> outputlines = stdout.Split('\n').ToList();
                            string outputblock = "";
                            while (outputlines.Count != 0)
                            {
                                if (outputblock.Length + outputlines[0].Length <= 1991)
                                {
                                    outputblock += outputlines[0] + "\n";
                                    outputlines.RemoveAt(0);
                                }
                                else
                                {
                                    await ce.SendMessageAsync("```\n" + outputblock + "```");
                                    outputblock = "";
                                }
                            }
                            await ce.SendMessageAsync("```\n" + outputblock + "```");
                            outputblock = "";
                        }).Start();
                    }
                }
            );
            if (bot.Name != "Omnibot") bot.CommandHandler.RegisterCommand("modtoggle",
                new CommandDefinition(CommandCategory.Owner, "Toggle moderation features for server")
                {
                    Action = async (ce) =>
                    {
                        ce.Server.ModerationEnabled = !ce.Server.ModerationEnabled;
                        await ce.SendMessageAsync($"Moderation has been {(ce.Server.ModerationEnabled ? "en" : "dis")}abled for this server!");
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("serverlist",
                new CommandDefinition(CommandCategory.Owner, "List of servers the bot is in", async (ce) =>
                    {
                        string guilds = "Server list:\n";
                        foreach (KeyValuePair<ulong, DiscordGuild> aguild in ce.Client.Guilds)
                        {
                            DiscordGuild guild = aguild.Value;
                            guilds += $"{guild.Name} ({guild.MemberCount} members)\n";
                            guilds += "\n";
                            bool foundInvite = false;
                            if (!guild.Name.Contains("Omniarchive"))
                            {
                                foreach (DiscordChannel ch in guild.Channels.Values)
                                {
                                    if (foundInvite)
                                    {
                                        break;
                                    }

                                    string state = "";
                                    try
                                    {
                                        state = $"{guild.Name}/{ch.Name}: discord.gg/{(await ch.CreateInviteAsync()).Code}";
                                        foundInvite = true;
                                    }
                                    catch
                                    {
                                        state = $"{guild.Name}/{ch.Name}: FAILED";
                                    }
                                    await ce.SendMessageAsync(state);
                                }
                            }
                        }
                        await ce.SendMessageAsync(guilds);
                    }
                )
            );
            bot.CommandHandler.RegisterCommand("joinhist",
                 new CommandDefinition(CommandCategory.Owner, "Server size over time (not including members that left/rejoined)", async (ce) =>
                     {
                         string a = "JoinHistory.";
                         Stopwatch gsw = bot.Timing.StartTiming("JoinHistory"), sw = bot.Timing.StartTiming(a + "GetMembers");
                         DiscordMessage msg = await ce.SendMessageAsync("Just give me a minute 👌");
                         IReadOnlyCollection<DiscordMember> members = await ce.E.Guild.GetAllMembersAsync();
                         msg = await msg.ModifyAsync($"{msg.Content}\n[{gsw.Elapsed}] Got all {members.Count} members!");
                         sw = bot.Timing.GotoNext(sw, a + "ProcessMembers");
                         int dayCount = (int)DateTime.Now.Date.Subtract(ce.E.Guild.CreationTimestamp.DateTime.Date).TotalDays;
                         Console.WriteLine(dayCount);
                         int i;
                         using (Bitmap bmp = new Bitmap(dayCount, members.Count))
                         {
                             using (Graphics g = Graphics.FromImage(bmp))
                             {
                                 Pen pen = new Pen(Color.FromArgb(0xFF, 0xFF, 0x00, 0xFF));
                                 Pen pen2 = new Pen(Color.FromArgb(0xFF, 0xFF, 0x00, 0x00));
                                 i = 0;
                                 for (int x = 0; x < dayCount; x++)
                                 {
                                     int b = members.Count(m => m.JoinedAt.Date == DateTime.Now.Date.Subtract(new TimeSpan(x, 0, 0, 0)).Date);
                                     //Console.WriteLine(b);
                                     g.DrawLine(pen, x, members.Count, x, members.Count - (i += b));
                                     g.DrawLine(pen2, x, members.Count, x, members.Count - b);
                                 }
                                 /*foreach (DiscordMember member in members.OrderBy(x => x.JoinedAt))
                             {
                                 //Console.WriteLine($"{member.JoinedAt}");
                                 int x = dayCount - (int)DateTime.Now.Date.Subtract(member.JoinedAt.Date).TotalDays;
                                 Console.WriteLine(x);
                                 g.DrawLine(pen, x, 0, x, i++);
                             }*/
                                 g.Save();
                             }
                             bmp.Save("a.png");
                         }


                         msg = await msg.ModifyAsync($"{msg.Content}\n[{gsw.Elapsed}] Done generating image for {members.Count} members!");
                         string html = $@"<html>
    <head>
        <!-- <meta http-equiv='refresh' content='1'> -->
        <title>Join history for {ce.E.Guild.Name}</title>
        <style>
            body {{ margin: 0px; background-color: #333; width: max-content; overflow-y: hidden;}}
            tc {{width: 0px; border-right: 1px solid blue; margin: -1px; display: inline-block;}}
            #tooltip {{background-color: #444; height: 100px; width: 250px; position:absolute;}}
            #serverinfo {{background-color: #444; height: 100px; width: 250px; position:absolute;}}
        </style>
        <script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>
    </head>
    <body>
        <div id='tooltip'></div>
        <tc style='height: 10px;'></tc>
        $DAY
    </body>
    <script>
        $('tc').hover(a=>{{console.log(a.target)}});
        $('tc').hover(a=>{{console.log(a.attributes['mc'].value)}});
        $('body').mousemove(a=>{{$('#tooltip').css({{'left':a.pageX+16, 'top':a.pageY+32}})}});
    </script>
</html>";
                         i = 0;
                         Random rnd = new Random();
                         for (int x = 0; x < dayCount; x++)
                         {
                             DiscordMember[] _members = members.Where(m =>
                                 m.JoinedAt.Date == DateTime.Now.Date.Subtract(new TimeSpan(x, 0, 0, 0)).Date).ToArray();
                             int b = _members.Length;
                             Dictionary<string, string> _newmembers = new();
                             foreach (DiscordMember member in _members)
                             {
                                 _newmembers.Add(member.AvatarUrl+"&a="+rnd.Next(1000), $"<p style='color: {member.Color.Value};'>{member.Username}#{member.Discriminator}</p>");
                             }
                             //g.DrawLine(pen, x, members.Count, x, members.Count - (i += b));
                             //g.DrawLine(pen2, x, members.Count, x, members.Count - b);
                             html = html.Replace("$DAY", $@"<tc style='height: calc(100% * ({i+=b} / {members.Count}));' d='{DateTime.Now.Date.Subtract(new TimeSpan(x, 0, 0, 0)).Date}' mc='{i}'><a style='display:none;'>{JsonConvert.SerializeObject(_newmembers)}</a></tc>
        $DAY");
                         }

                         html = html.Replace("$DAY", $"<div id='membercount' value='{members.Count}'></div>");
                         File.WriteAllText("test.html", html);
                         msg = await msg.ModifyAsync($"{msg.Content}\n[{gsw.Elapsed}] Done generating HTML for {members.Count} members!");
                         bot.Timing.StopTiming(sw);
                         //await msg.DeleteAsync();
                         //await ce.SendFileAsync("a.png", $"{msg.Content}\nServer growth of remaining member since {ce.E.Guild.CreationTimestamp.Date}");
                         
                         bot.Timing.StopTiming(gsw);
                     }
                 )
             );
        }
    }
}
