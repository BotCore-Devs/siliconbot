﻿using BotCore.DataGenerators;
using BotCore.Structures.Base;

using CoreBot.Structures.Command;

using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Z.Expressions;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgDeveloper
    {
        public static void SetupCommands(Bot bot)
        {
            if (bot.Name != "Omnibot") bot.CommandHandler.RegisterCommand("timings", new CommandDefinition(CommandCategory.Developer, "Average timings",
                async (ce) =>
                {
                    ConcurrentDictionary<string, Stopwatch> timings = new ConcurrentDictionary<string, Stopwatch>();
                    ConcurrentDictionary<string, double> dtiming = new ConcurrentDictionary<string, double>();
                    ConcurrentDictionary<string, int> dtimingentries = new ConcurrentDictionary<string, int>();
                    Parallel.ForEach(bot.Timing.Timings, (tim) =>
                    {
                        //string n = Regex.Replace(tim.Key, @"[\d-]", string.Empty).Replace("__","_");
                        //timings.TryAdd(n, tim.value);
                        /*if (tim.Key.StartsWith("Message") && !tim.Key.Contains('.') && !tim.Value.IsRunning) */
                        if (!tim.Value.IsRunning)
                        {
                            timings.TryAdd(tim.Key, tim.Value);
                        }
                    });
                    string moredetails = "```diff\n";
                    Parallel.ForEach(bot.Timing.Timings, (tim) =>
                    {
                        if ( /*(tim.Key.StartsWith("Message") || tim.Key.StartsWith("GenLeaderboard")) && tim.Key.Contains('.') && */ !tim.Value.IsRunning)
                        {
                            dtiming.AddOrUpdate(
                                /*tim.Key.Split('.').Last(),*/
                                Regex.Replace(tim.Key, @"[\d-]", string.Empty).Replace("__", "_"),
                                tim.Value.Elapsed.Ticks / (double)TimeSpan.TicksPerMillisecond,
                                (_, old) => old + tim.Value.Elapsed.Ticks / (double)TimeSpan.TicksPerMillisecond
                            );
                            dtimingentries.AddOrUpdate( /*tim.Key.Split('.').Last(),*/ Regex.Replace(tim.Key, @"[\d-]", string.Empty).Replace("__", "_"), 1, (_, old) => old + 1);
                        }
                    });

                    foreach (KeyValuePair<string, double> data in dtiming.OrderByDescending(key => key.Value))
                    {
                        moredetails += $"- {data.Key}: ~{Math.Round(data.Value / dtimingentries[data.Key], 3)} ms/run\n    ({dtimingentries[data.Key]}x -> {Math.Round(data.Value, 3)} ms)\n";
                    }
                    moredetails += "```";

                    double total = 0;
                    foreach (double t in dtiming.Values)
                    {
                        total += t;
                    }
                    double avgpermessage = 0;
                    Parallel.ForEach(timings, (tim) => { avgpermessage += tim.Value.Elapsed.Ticks / (double) TimeSpan.TicksPerMillisecond; });
                    string tmsg = $"Average timings:\n\n" +
                                  $"Uptime: {DateTime.Now.Subtract(Process.GetCurrentProcess().StartTime).Duration()}\n" +
                                  $"Total time spent: {Math.Round(total, 3)} ms\n" +
                                  $"Message count: {ce.Server.MessageCount}\n" +
                                  $"Average time per message: {Math.Round(avgpermessage / timings.Count, 3)}\n" +
                                  moredetails;
                    Dictionary<string, SmallSw> st = new Dictionary<string, SmallSw>();
                    foreach (KeyValuePair<string, Stopwatch> sw in bot.Timing.Timings)
                    {
                        st.Add(sw.Key, new SmallSw() { Elapsed = sw.Value.Elapsed.ToString() });
                    }

                    new TimingsGraphGenerator(bot).Generate();

                    bool success = false;
                    while (!success)
                    {
                        try
                        {
                            await ce.SendFileAsync("test.png", tmsg);
                            success = true;
                        }
                        catch { Thread.Sleep(100); }
                    }
                    st.SaveToJsonFile($"{bot.DataDir}/Timings.json");
                }));
            if (bot.Name != "Omnibot") bot.CommandHandler.RegisterCommand("exec",
                new CommandDefinition(CommandCategory.Developer, "Run a payload", async (ce) =>
                {
                    Console.WriteLine(string.Join(", ", ce.Args));
                    if (ce.Args.Count == 0)
                    {
                        await ce.SendMessageAsync("No payload name given!");
                    }
                    else
                    {
                        switch (ce.Args[0])
                        {
                            default:
                                await ce.SendMessageAsync("Payload not found!");
                                break;
                        }
                    }
                })
            );
            if (bot.Name != "Omnibot") bot.CommandHandler.RegisterCommand("eval",
                new CommandDefinition(CommandCategory.Developer, "Evaluate C# code", async (ce) =>
                {
                    try
                    {
                        string code = ce.E.Message.Content.Replace(ce.Server.Prefix + "eval ", "");
                        int i = code.IndexOf("\n", StringComparison.Ordinal);
                        code = code[i..];
                        i = code.LastIndexOf("```", StringComparison.Ordinal);
                        code = code.Substring(0, i);

                        try
                        {
                            await ce.SendMessageAsync(
                                $"Code output (Z.Expressions.Eval):\n\n{Eval.Execute(code, new EvalEnv(){bot = bot, ce = ce})}");
                        }
                        catch (Exception E)
                        {
                            await ce.SendMessageAsync($"Something has gone wrong whilst executing your code (Z.Expressions.Eval)!```cs\n{E}```");   
                        }
                    }
                    catch (Exception e)
                    {
                        await ce.SendMessageAsync($"Something has gone wrong whilst executing your code!```cs\n{e}```");
                    }
                })
            );
        }
    }

    internal class SmallSw
    {
        public string Elapsed = "";
    }

    internal class EvalEnv
    {
        public Bot bot;
        public CommandEnvironment ce;
    }
}
