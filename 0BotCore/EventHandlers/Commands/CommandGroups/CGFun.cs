﻿using BotCore.Structures.Base;

using CoreBot.Structures.Command;
using System.IO;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgFun
    {
        public static void SetupCommands(Bot bot)
        {

            // Avatar Command
            // FUNCTION: Returns the avatar of the user that runs this command
            bot.CommandHandler.RegisterCommand("avatar",
                // Command Definition in Help Menu
                new CommandDefinition(CommandCategory.Fun, "Returns the avatar of the mentioned user", async (ce) =>
                {
                    // If the amount of users mentioned is greater than one send the get and send the avatars of the mentioned users
                    if (ce.E.MentionedUsers.Count >= 1)
                    {
                        await ce.SendMessageAsync(ce.E.MentionedUsers[0].AvatarUrl.Split('?')[0] + "?size=2048");
                    }

                    // The amount of users mentioned was only 1
                    else
                    {
                        await ce.SendMessageAsync(ce.E.Author.AvatarUrl.Split('?')[0] + "?size=2048");
                    }
                })
            );
            if (bot.Name == "Omnibot") return;
            // Senko Noises Command, decided to reimplement this because why not?
            bot.CommandHandler.RegisterCommand("senkonoises",
            new CommandDefinition(CommandCategory.Fun, "Yes... It's noises... That's about it", async (ce) =>
                {
                    await ce.SendMessageAsync("https://www.youtube.com/watch?v=YhCnMCyi8Z8");
                }
            ));

            // Floof Command
            bot.CommandHandler.RegisterCommand("floof",
                // Command Definition in Help Menu
                new CommandDefinition(CommandCategory.Fun, "Floof Pics... Yes", async (ce) =>
                {
                    // If a folder by the name of "floof" exists, get all files by the name of "floof" and send one of them in the channel where this command was ran
                    if (Directory.Exists("floof"))
                    {
                        // Get files by the name of "floof"
                        string[] files = Directory.GetFiles("floof");
                        // Send it
                        await ce.SendFileAsync(files[RuntimeInfo.Rnd.Next(files.Length)]);
                    }

                    // The folder does not exist
                    else
                    {
                        await ce.SendMessageAsync("Could not find floof folder! :c");
                    }
                })
            );

            /* This Command is a direct ripoff per se of the floof command, except replace the images with Headpats pls (also make a folder by the name of headpats) */
            // Headpat Command
            bot.CommandHandler.RegisterCommand("headpat",
                // Command Definition in Help Menu
                new CommandDefinition(CommandCategory.Fun, "Headpats for everyone!", async (ce) =>
                {
                    // If a directory by the name "headpats" exists
                    if (Directory.Exists("headpats"))
                    {
                        // Get files that go by the name "headpats"
                        string[] files = Directory.GetFiles("headpats");
                        // Send/Upload the File to the channel where the command was ran
                        await ce.SendFileAsync(files[RuntimeInfo.Rnd.Next(files.Length)]);
                    }

                    // Else the headpats folder was not found
                    else
                    {
                        await ce.SendMessageAsync("Could not find the headpats folder! :c");
                    }
                })
            );

            // Isn't this command premium only?
            // Spray Command
            bot.CommandHandler.RegisterCommand("spray",
                // Command Definition in Help Menu
                new CommandDefinition(CommandCategory.Fun, "BAD! UwU", async (ce) =>
                {
                    // If a file by the name "spray_face.jpg" exists, send it in the channel where this command was ran
                    if (File.Exists("spray_face.jpg"))
                    {
                        // Send/Upload "spray_face.jpg" in the channel where this command was ran
                        await ce.SendFileAsync("spray_face.jpg", $"{ce.Message}\n\n     - {ce.E.Author.Username}");
                    }

                    // The file was not found
                    else
                    {
                        await ce.SendMessageAsync("File does not exist!");
                    }
                }));
        }
    }
}
