﻿using BotCore.Structures.Base;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgMusic
    {
        public static void SetupCommands(Bot bot)
        {
#if false
            bot.commandHandler.RegisterCommand("summon",
                new CommandDefinition()
                {
                    Category = CommandCategory.Music,
                    Description = "Summons bot to your current voice chat",
                    Action = new Action<CommandEnvironment>(async (ce) =>
                    {
                        VoiceNextExtension vne = ce.Client.GetVoiceNext();
                        if (vne.GetConnection(ce.e.Guild) != null) await ce.SendMessageAsync("Already connected in this server!");
                        else
                        {
                            if (ce.e.Member().VoiceState.Channel == null) await ce.SendMessageAsync("You must be connected to a voice channel!");
                            else
                            {
                                await vne.ConnectAsync(ce.e.Member().VoiceState.Channel);
                                await ce.SendMessageAsync("Connected!");
                            }
                        }

                    })
                }
            );
            bot.commandHandler.RegisterCommand("leave",
                new CommandDefinition()
                {
                    Category = CommandCategory.Music,
                    Description = "Leaves current voice chat",
                    Action = new Action<CommandEnvironment>(async (ce) =>
                    {
                        VoiceNextExtension vne = ce.Client.GetVoiceNext();
                        if (vne.GetConnection(ce.e.Guild) == null) await ce.SendMessageAsync("Not connected in this server!");
                        else
                        {
                            vne.GetConnection(ce.e.Guild).Disconnect();
                            await ce.SendMessageAsync("Disconnected!");
                            try
                            {
                                Directory.Delete($"Music/{ce.e.Guild.Id}", true);
                            }
                            catch
                            {
                                await ce.SendMessageAsync("Failed to clear queue!");
                            }
                        }

                    })
                }
            );
            /* TODO: Implement Proper Queuing System, procrastination ensues... */
            bot.commandHandler.RegisterCommand("queue",
                new CommandDefinition()
                {
                    Category = CommandCategory.Music,
                    Description = "Show or add song to queue",
                    Action = new Action<CommandEnvironment>((ce) =>
                    {
                        if (ce.Args.Count == 0)
                        {
                            //unused?
                            //string queue = "";
                            //int i = 0;
                            //await ce.SendMessageAsync($"Queue:```{ String.Join($"\n    {i++}. ",ServerMusicTracking.GetPlayer(ce.e.Guild.Id).queue.queue.ToArray())}```");
                        }
                        else
                        {

                        }

                    })
                }
            );
            bot.commandHandler.RegisterCommand("play",
                new CommandDefinition()
                {
                    Category = CommandCategory.Music,
                    Description = "Leaves current voice chat",
                    Action = new Action<CommandEnvironment>(async (ce) =>
                    {
                        ServerMusicPlayer mp = ServerMusicTracking.GetPlayer(ce.e.Guild.Id);
                        if (ce.e.Member().VoiceState == null) await ce.SendMessageAsync("You must be connected to a voice channel!");
                        else
                        if (ce.Args[0].Contains("youtube.com/playlist"))
                        {
                            YoutubeClient cli = new YoutubeClient();
                            var pl = await cli.Playlists.GetVideosAsync("PLFr3c472VstyuniFqFD6KBtzQhLuT_BKO");
                            foreach (Video v in pl)
                            {
                                await mp.queue.Add(v.Url);
                            }
                            await ce.SendMessageAsync($"Queued {pl.Count} videos from playlist");
                        }
                        else
                        {

                        }
                        mp.Play(ce);
                    })
                }
            );
            bot.commandHandler.RegisterCommand("lplay",
                new CommandDefinition()
                {
                    Category = CommandCategory.Owner,
                    Description = "Play local file in vc",
                    Action = new Action<CommandEnvironment>(async (ce) =>
                    {
                        VoiceNextExtension vne = ce.Client.GetVoiceNext();
                        if (vne.GetConnection(ce.e.Guild) != null) await ce.SendMessageAsync("Already connected in this server!");
                        else
                        {
                            if (ce.e.Member().VoiceState.Channel == null) await ce.SendMessageAsync("You must be connected to a voice channel!");
                            else
                            {
                                await vne.ConnectAsync(ce.e.Member().VoiceState.Channel);
                                await ce.SendMessageAsync("Connected!");
                            }
                        }

                        string file = ce.e.Message.Content.Replace("<<lplay ", "");
                        if (vne.GetConnection(ce.e.Guild) == null) await ce.SendMessageAsync("Not connected in this server!");
                        else
                        {
                            await ce.SendMessageAsync("Playing!");
                            var vnc = vne.GetConnection(ce.e.Guild);
                            await vnc.SendSpeakingAsync(true);
                            var psi = new ProcessStartInfo
                            {
                                FileName = "ffmpeg",
                                Arguments = $@"-i ""{file}"" -ac 2 -f s16le -ar 48000 pipe:1",
                                RedirectStandardOutput = true,
                                UseShellExecute = false
                            };
                            var ffmpeg = Process.Start(psi);
                            var ffout = ffmpeg.StandardOutput.BaseStream;

                            var txStream = vnc.GetTransmitStream();
                            await ffout.CopyToAsync(txStream);
                            await txStream.FlushAsync();

                            await vnc.SendSpeakingAsync(false); // we're not speaking anymore
                        }

                    })
                }
            );
#endif
        }
    }
}
