﻿using BotCore.Structures.Base;

using DSharpPlus.Entities;

using System;
using System.IO;
using System.Linq;

namespace CoreBot
{
    internal class Util
    {
        public static ulong ParseTime(string time)
        {
            string timestring = time[0..^1];
            if (ulong.TryParse(timestring, out ulong seconds))
            {
                return time.Last() switch
                {
                    's' => seconds,
                    'm' => seconds * 60,
                    'h' => seconds * 60 * 60,
                    'd' => seconds * 60 * 60 * 24,
                    'w' => seconds * 60 * 60 * 24 * 7,
                    'o' => seconds * 60 * 60 * 24 * 30,
                    'y' => seconds * 60 * 60 * 24 * 365,
                    _ => seconds
                };
            }
            
            return seconds;
        }
        public static void PrintSplit(string charBet, params object[] parts)
        {
            foreach (object part in parts)
            {
                Console.Write(part);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(charBet);
                Console.ResetColor();
            }
        }
        public static int GetUserCount(Bot bot)
        {
            int users = 0;
            foreach (DiscordGuild guild in bot.Client.Guilds.Values)
            {
                users += guild.MemberCount;
            }

            return users;
        }
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}
