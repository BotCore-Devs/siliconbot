﻿using BotCore.Structures.Base;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Unidecode.NET;

namespace CoreBot
{
    public static class Extensions
    {
        public static DiscordMember Member(this MessageCreateEventArgs e) //get Member object from MessageCreateEvent
        {
            if (e.Author.Discriminator == "0000") return null;
            return e.Guild.Members.ContainsKey(e.Author.Id) ? e.Guild.Members[e.Author.Id] : e.Guild.GetMemberAsync(e.Author.Id).Result;
        }
        public static DiscordMember Member(this DiscordMessage msg)
        {
            return msg.Channel.Guild.Members[msg.Author.Id];
        }
        public static string DisplayName(this DiscordMember user) //get name displayed in client
        {
            return user.Nickname ?? user.Username;
        }
        public static bool ContainsAnyCase(this string str, string test) //check if lowercase string contains variable "test" (should be lowercase, otherwise always returns false)
        {
            return str.ToLower().Contains(test);
        }
        public static bool ContainsAnyOf(this string str, IEnumerable<string> test) //check if string contains any lowercase instance of any item
        {
            foreach (string item in test)
            {
                if (str.ToLower().Contains(item.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool StartsWithAnyOf(this string str, IEnumerable<string> test) //check if string starts with any lowercase instance of any item
        {
            foreach (string item in test)
            {
                if (str.ToLower().StartsWith(item.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool UserHasPermission(this DiscordChannel channel, Permissions permission, DiscordMember member)
        {
            if (channel == null) return false;
            return member.PermissionsIn(channel).HasFlag(permission);
        }
        public static string ApplyVars(this string template, Bot bot)
        {
            return template
                .Replace("$USERS", Util.GetUserCount(bot) + "")
                .Replace("$VER", RuntimeInfo.Ver);
        }
        public static void Log(this Exception e) //log errors
        {
            try
            {
                Debug.WriteLine(e);
            }
            catch
            {
                try
                {
                    Debug.WriteLine(e);
                }
                catch
                {
                    Console.WriteLine("Excuse me what the fuck? Failed to send error");
                    Debug.WriteLine(e);
                }
            }
        }
        public static string ContentOrEmtpy(this string str) //check empty string if null
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            else
            {
                return str;
            }
        }

        public static void SetStatus(string status)
        {
            //Console.Title = $"{(bot.Name ?? "Unknown bot")} | {Status} | Uptime since event start: {(DateTime.Now - Process.GetCurrentProcess().StartTime).TotalMilliseconds} ms";
        }
        public static void SaveToJsonFile(this object @object, string filename) // save object to json file
        {
            // serialise object
            string json = JsonConvert.SerializeObject(@object, Formatting.Indented, new JsonSerializerSettings()
            {
                DefaultValueHandling = DefaultValueHandling.Populate
            });
            // save to files
            try
            {
                File.WriteAllText(filename, json);
            }
            catch
            {
                // ignored
            }
        }
        public static int CountInstances(this string haystack, string needle)
        {
            return haystack.Select((_, i) => haystack[i..]).Count(sub => sub.StartsWith(needle));
        }
        public static int CountInstancesOfAll(this string haystack, IEnumerable<string> needles)
        {
            int instances = 0;
            foreach (string needle in needles)
            {
                instances += haystack.CountInstances(needle);
            }

            return instances;
        }
        public static string Toggle(this ref bool @bool)
        {
            // ReSharper disable once AssignmentInConditionalExpression
            return (@bool = !@bool) ? "enabled" : "disabled";
        }
        public static string RemoveDiacritics(this string text)
        {
            string normalizedString = text.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            foreach (char c in normalizedString)
            {
                UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
        public static string ToAlphaNumeric(this string text)
        {
            return text.Unidecode();
        }
    }
}
