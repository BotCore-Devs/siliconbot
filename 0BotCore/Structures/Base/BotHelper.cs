﻿using BotCore.Structures.Base;

using DSharpPlus;

using System.Diagnostics;
using System.IO;

namespace CoreBot
{
    internal static class BotHelper
    {
        public static bool BotWebInit = false;
        public static Bot BotInit;
        public static void GetBaseDiscordClient(Bot bot)
        {
            Stopwatch sw = bot.Timing.StartTiming($"[{bot.Name}] GetBaseDiscordClient");
            DiscordConfiguration cfg = new DiscordConfiguration
            {
                Token = File.Exists($"/auth/{bot.Name}/token.txt") ? File.ReadAllText($"/auth/{bot.Name}/token.txt") : BotManager.DefaultToken,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                ReconnectIndefinitely = true,

            };
            bot.Client = new DiscordClient(cfg);
            bot.Client.GuildAvailable += bot.GuildAvailableHandler.HandleGuildAvailable;
            bot.Client.MessageCreated += bot.MessageHandler.HandleMessage;
            bot.Client.GuildMemberAdded += bot.GuildMemberAddedHandler.HandleNewMember;
            bot.Client.MessageReactionAdded += bot.MessageReactionAddedHandler.HandleReaction;

            bot.Timing.StopTiming(sw);
        }
    }
}
