﻿using BotCore.DataGenerators;
using BotCore.DataGenerators.Web;
using BotCore.EventHandlers;
using BotCore.Structures.ServiceWorkers;
using BotCore.WebServices_ASP;

using CoreBot;
using CoreBot.EventHandlers;

using DSharpPlus;
using DSharpPlus.Entities;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using Omnibot.EventHandlers;

namespace BotCore.Structures.Base
{
    public class Bot
    {
        public DiscordClient Client;
        public string Name { get; set; }
        public string DataDir => (BotManager.IsLinux ? "/data" : "Config" + (BotManager.IsSingleBot ? "":$"/{Name}"));
        public bool HasWebServer = true;
        public int Port = -1;
        public List<DiscordActivity> Statusses = new()
        {
            new DiscordActivity("$VER"),
            new DiscordActivity("with $USERS members"),
            new DiscordActivity("over $USERS members", ActivityType.Watching),
            new DiscordActivity("https://discord.gg/VwKMryw"),
            //covid-19
            new DiscordActivity("Stay home!"),
            new DiscordActivity("Wash your hands!"),
            new DiscordActivity("Keep distance!"),
            new DiscordActivity("Don't touch your face!"),
            new DiscordActivity("Play some games!")
        };


        public readonly Timing Timing;
        public readonly LeaderboardGenerator LeaderboardGenerator;
        public readonly BehaviorBoardGenerator BehaviorBoardGenerator;
        public UserPageGenerator UserPageGenerator;
        public readonly MessageHandler MessageHandler;
        public readonly CommandHandler CommandHandler;
        public readonly GuildAvailableHandler GuildAvailableHandler;
        public readonly GuildMemberAddedHandler GuildMemberAddedHandler;
        public readonly MessageReactionAddedHandler MessageReactionAddedHandler;
        public readonly ContentFilterWorker ContentFilterWorker;
        public readonly TimingsGraphGenerator TimingsGraphGenerator;
        //legacy data storage
        public LegacyDataManager DataManager;
        public LegacyDataStore DataStore;
        //timed event handler
        public TimedEventManager TimedEventManager;

        public DateTime LastUpdated = new();
        public int Totalusers = 0, Onlineusers = 0;

        //ctr
        public Bot(string name)
        {
            //cd into bot dir
            Environment.CurrentDirectory = new FileInfo(Assembly.GetAssembly(typeof(Bot))?.Location).DirectoryName;
        
            BotManager.Bots.Add(name, this);
            Name = name;
            Timing = new();
            LeaderboardGenerator = new(this);
            BehaviorBoardGenerator = new(this);
            DataManager = new(this);
            DataStore = DataManager.Load();
            CommandHandler = new(this);
            GuildAvailableHandler = new(this);
            MessageHandler = new(this);
            GuildMemberAddedHandler = new(this);
            ContentFilterWorker = new(this);
            MessageReactionAddedHandler = new(this);
            TimingsGraphGenerator = new(this);
            TimedEventManager = new(this);

            BotHelper.GetBaseDiscordClient(this);
        }
        public void Start()
        {
            if (HasWebServer)
            {
                while (BotHelper.BotWebInit) Thread.Sleep(100);
                BotHelper.BotInit = this;
                new Thread(() => WebProvider.Init(this)).Start();
            }
            CommandHandler.Init();
            Client.ConnectAsync();
        }
        public void Stop()
        {
            Client.DisconnectAsync();
            Client.Dispose();
        }
    }
}
