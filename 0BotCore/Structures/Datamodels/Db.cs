﻿
using Microsoft.EntityFrameworkCore;

namespace SiliconBotCore3
{
    public partial class Db : DbContext
    {
        public Db() { }

        public Db(DbContextOptions<Db> options) : base(options) { }

        public virtual DbSet<DServerUser> DServerUser { get; set; }
        public virtual DbSet<DataStoreCollection> DataStoreCollection { get; set; }
        public virtual DbSet<GlobalUser> GlobalUser { get; set; }
        public virtual DbSet<Level> Level { get; set; }
        public virtual DbSet<Quote> Quote { get; set; }
        public virtual DbSet<Server> Server { get; set; }
        public virtual DbSet<Warning> Warning { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=thearcanebrony.net;Database=bottest;Username=bots;Password=bot");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DServerUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("user_pkey");

                entity.ToTable("d_server_user");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasDefaultValueSql("nextval('user_user_id_seq'::regclass)");

                entity.Property(e => e.DServerId).HasColumnName("d_server_id");

                entity.Property(e => e.DUserId).HasColumnName("d_user_id");

                entity.HasOne(d => d.DServer)
                    .WithMany(p => p.DServerUser)
                    .HasForeignKey(d => d.DServerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d_server_user_fk");
            });

            modelBuilder.Entity<DataStoreCollection>(entity =>
            {
                entity.HasKey(e => e.BotId);

                entity.ToTable("dataStoreCollection");

                entity.Property(e => e.BotId).HasColumnName("BotID");
            });

            modelBuilder.Entity<GlobalUser>(entity =>
            {
                entity.ToTable("global_user");

                entity.Property(e => e.GlobalUserId).HasColumnName("global_user_id");
            });

            modelBuilder.Entity<Level>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("level");

                entity.Property(e => e.DServerId).HasColumnName("d_server_id");

                entity.Property(e => e.Level1)
                    .HasColumnName("level")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LevelId)
                    .HasColumnName("level_id")
                    .HasColumnType("character varying");

                entity.Property(e => e.Multiplier)
                    .HasColumnName("multiplier")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Xp)
                    .HasColumnName("xp")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.User)
                    .WithMany()
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("level_fk");
            });

            modelBuilder.Entity<Quote>(entity =>
            {
                entity.ToTable("quote");

                entity.Property(e => e.QuoteId).HasColumnName("quote_id");

                entity.Property(e => e.AuthorId).HasColumnName("author_id");

                entity.Property(e => e.MessageText)
                    .HasColumnName("message_text")
                    .HasMaxLength(2000);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Quote)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("quote_fk");
            });

            modelBuilder.Entity<Server>(entity =>
            {
                entity.ToTable("server");

                entity.Property(e => e.ServerId).HasColumnName("server_id");

                entity.Property(e => e.DServerId).HasColumnName("d_server_id");
            });

            modelBuilder.Entity<Warning>(entity =>
            {
                entity.ToTable("warning");

                entity.Property(e => e.WarningId).HasColumnName("warning_id");

                entity.Property(e => e.AuthorId).HasColumnName("author_id");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(1500);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.WarningAuthor)
                    .HasForeignKey(d => d.AuthorId)
                    .HasConstraintName("warning_fk_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WarningUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("warning_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
