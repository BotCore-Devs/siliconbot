﻿namespace SiliconBotCore3
{
    public partial class Warning
    {
        public long WarningId { get; set; }
        public long? UserId { get; set; }
        public long? AuthorId { get; set; }
        public string Message { get; set; }

        public virtual DServerUser Author { get; set; }
        public virtual DServerUser User { get; set; }
    }
}
