﻿// ReSharper disable PartialTypeWithSinglePart
namespace SiliconBotCore3
{
    public partial class Quote
    {
        public long QuoteId { get; set; }
        public string MessageText { get; set; }
        public long? AuthorId { get; set; }

        public virtual DServerUser Author { get; set; }
    }
}
