﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using CoreBot;
using CoreBot.EventHandlers.Commands;
using DSharpPlus.VoiceNext;
using YoutubeExplode;

namespace BotCore.Structures.RuntimeStorage
{
    static class ServerMusicTracking
    {
        private static Dictionary<ulong, ServerMusicPlayer> _players = new Dictionary<ulong, ServerMusicPlayer>();
        public static ServerMusicPlayer GetPlayer(ulong guildId)
        {
            ServerMusicPlayer player = new ServerMusicPlayer();
            if (!_players.ContainsKey(guildId)) _players.Add(guildId, player);
            else player = _players[guildId];
            return player;
        }
    }
    class ServerMusicPlayer
    {
        public ServerMusicPlayerQueue Queue = new();
        public VoiceNextConnection Vnc;
        public bool IsPlaying = false;
        public async void Play(CommandEnvironment ce)
        {
            VoiceNextExtension vne = ce.Client.GetVoiceNext();
            if (Vnc == null)
            {
                await ce.SendMessageAsync("Not connected?");
            }
            if (!IsPlaying)
            {
                //VoiceNextConnection vnc;
                if (ce.E.Member().VoiceState == null) await ce.SendMessageAsync("You must be connected to a voice channel!");
                else
                {
                    Vnc = await vne.ConnectAsync(ce.E.Member().VoiceState.Channel);
                    await ce.SendMessageAsync("Connected!");
                }

                //string file = ce.E.Message.Content.Replace(ce.Server.Prefix + "play ", "");
                if (vne.GetConnection(ce.E.Guild) == null) await ce.SendMessageAsync("Not connected in this server!");
                else
                {
                    MusicItem musicItem;
                    while ((musicItem = Queue.Next()) != null)
                    {
                        Vnc = vne.GetConnection(ce.E.Guild);
                        while (Vnc.IsPlaying) Thread.Sleep(100);
                        //if (!Directory.Exists("Music")) Directory.CreateDirectory("Music");
                        //if (!Directory.Exists("Music/" + ce.E.Guild.Id)) Directory.CreateDirectory("Music/" + ce.E.Guild.Id);
                        Vnc = vne.GetConnection(ce.E.Guild);
                        var txStream = Vnc.GetTransmitSink();
                        txStream.VolumeModifier = 0.75;

                        await ce.SendMessageAsync("Downloading!");
                        var ytdlpsi = new ProcessStartInfo
                        {
                            FileName = "youtube-dl",
                            Arguments = $@"-ci -f best ""{musicItem.Url}"" -o - --quiet", //Music/{ce.E.Guild.Id}/{Directory.GetFiles($"Music/{ce.E.Guild.Id}").Length}",
                            RedirectStandardOutput = true,
                            UseShellExecute = false
                        };
                        var ytdl = Process.Start(ytdlpsi);
                        var psi = new ProcessStartInfo
                        {
                            FileName = "ffmpeg",
                            Arguments = $@"-i - -ar 48000 -f wav -", //Music/{ce.E.Guild.Id}/0
                            RedirectStandardOutput = true,
                            UseShellExecute = false,
                            RedirectStandardInput = true
                        };
                        var ffmpeg = Process.Start(psi);

                        await ce.SendMessageAsync("Preparing!");
                        if (ytdl != null)
                            if (ffmpeg != null)
                                await ytdl.StandardOutput.BaseStream.CopyToAsync(ffmpeg.StandardInput.BaseStream);
                        if (ffmpeg != null)
                        {
                            var ffout = ffmpeg.StandardOutput.BaseStream;

                            await ce.SendMessageAsync($"Playing *{musicItem.Name}*!");
                            new Thread(async () => { while (!ffmpeg.HasExited) { await Vnc.SendSpeakingAsync(); Thread.Sleep(1000); } }).Start();

                            //while (!ffmpeg.HasExited) txStream.WriteByte((byte)ffout.ReadByte());
                            await ffout.CopyToAsync(txStream);
                        }

                        //await ffout.CopyToAsync(txStream);
                        await txStream.FlushAsync();
                        //ffmpeg.WaitForExit();
                        //while (vnc.IsPlaying) { Thread.Sleep(1000); }
                        await Vnc.SendSpeakingAsync(false);

                        await ce.SendMessageAsync("Finished playing!");
                    }
                }
            }
        }

        public void Pause()
        {

        }
        public void Skip()
        {

        }
    }
    class ServerMusicPlayerQueue
    {
        public async Task<ServerMusicPlayerQueue> Add(string url, CommandEnvironment ce = null)
        {
            MusicItem mi = new MusicItem() { Url = url };
            Dictionary<string, string> urlargs = new Dictionary<string, string>();
            if (url.Contains('?') && url.Contains('='))
            {
                foreach (string kvp in url.Split("?")[1].Split("&")) urlargs.Add(kvp.Split('=')[0], kvp.Split('=')[1]);
                url = url.Split('?')[0];
            }
            if (url.ContainsAnyOf(new[] { "youtu.be/", "youtube.com/watch?v=" }))
            {
                if (url.Contains("youtu.be/")) urlargs.Add("v", url.Split('/')[1]);
                var vid = await new YoutubeClient().Videos.GetAsync(urlargs["v"]);
                mi.Name = vid.Title;
                mi.Icon = vid.Thumbnails.HighResUrl;
            }
            Queue.Enqueue(mi);
            if (ce != null)
            {
                await ce.SendMessageAsync($"Queued {mi.Name}");
            }
            return this;
        }
        public MusicItem Next()
        {
            return Queue.Dequeue();
        }
        public readonly Queue<MusicItem> Queue = new Queue<MusicItem>();
    }
    class MusicItem
    {
        public string Url;
        public string Name = "Unknown";
        public string Icon = "";
    }
}
