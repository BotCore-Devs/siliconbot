﻿using BotCore.Structures.Base;
using BotCore.Structures.DbInterface;

using CoreBot.Structures.DataStorage;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CoreBot
{
    public class LegacyDataManager
    {

        public LegacyDataManager(Bot bot)
        {
            this.bot = bot;

            // load data store on initialisation
            DataStore = Load();
        }

        public LegacyDataStore DataStore;
        private Bot bot;

        public void Save()
        {
            try
            {
                // create config folder if it doesnt already exist
                if (!Directory.Exists(bot.DataDir))
                {
                    Directory.CreateDirectory(bot.DataDir);
                }

                //DataStore.Servers.SaveToJsonFile("Config/Servers.json");
                foreach ((ulong id, LegacyServer s) in DataStore.Servers)
                {
                    Directory.CreateDirectory($"{bot.DataDir}/{id}");
                    s.SaveToJsonFile($"{bot.DataDir}/{id}/Config.json");
                    foreach (LegacyUser u in s.Users.Values)
                    {
                        u.SaveToJsonFile($"{bot.DataDir}/{id}/{u.UserId}.json");
                    }
                }
            }
            catch (Exception e) { e.Log(); throw; }
        }
        public LegacyDataStore Load()
        {
            LegacyDataStore dataStore = new LegacyDataStore(bot);
            if (!Directory.Exists(bot.DataDir))
            {
                Directory.CreateDirectory(bot.DataDir);
            }
            return dataStore;
        }
    }
    public class LegacyDataStore
    {
        // stores global configuration data and LegacyServer list
        public Dictionary<ulong, LegacyServer> Servers = new Dictionary<ulong, LegacyServer>(); // Servers[ServerID] returns LegacyServer object, storing members and config
        private Bot bot;

        public LegacyDataStore(Bot bot)
        {
            this.bot = bot;
        }

        public async Task<LegacyServer> GetServer(ulong serverId) //returns user object for user id, creates if it doesnt exist
        {
            /*if (false && Servers.ContainsKey(serverId) && Servers.TryGetValue(serverId, out LegacyServer server) && server != null)
            {
                return server;
            }
            else*/
            {
                LegacyServer server = new LegacyServer(bot);
                string baseDir = $"{bot.DataDir}/{serverId}", filename = $"{baseDir}/Config.json";
                if (!Directory.Exists(baseDir))
                {
                    Directory.CreateDirectory(baseDir);
                }

                Debug.Write(filename + " ");
                Debug.WriteLine(File.Exists(filename));
                if (File.Exists(filename))
                {
                    try
                    {
                        JsonConvert.PopulateObject(await File.ReadAllTextAsync(filename), server);
                        //server = JsonConvert.DeserializeObject<LegacyServer>(File.ReadAllText(Filename));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine($"Failed to load config for {serverId}, restored defaults!");
                        File.Move(filename, filename + Directory.GetFiles(baseDir).Length);

                        try
                        {
                            server = JsonConvert.DeserializeObject<LegacyServer>(await File.ReadAllTextAsync("Backup_" + filename));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Failed to load backup config for {serverId}, reset to default!");
//                            File.Move(filename, filename + Directory.GetFiles(baseDir).Length);
                            throw;
                        }
                        throw;
                    }

                }
                else
                {
                    server.Save();
                }
                try
                {
                    if (bot.Client != null && serverId != 0)
                    {
                        //server.Guild = await bot.Client.GetGuildAsync(serverId);
                    }
                }
                catch
                {
                    Console.WriteLine($"[{bot.Name}] Failed to get guild with ID {serverId}!");
                }

                //Servers.TryAdd(ServerID, server);
                try
                {
                    Migrations.MigrateLegacyServerToDb(server);
                }
                catch (Exception er)
                {
                    Console.WriteLine("Something has gone wrong migrating to DB!");
                    Console.WriteLine(er);
                }
                server.ContentFilter.BadStrings = server.ContentFilter.BadStrings.Distinct().ToList();
                return server;
            }
        }
    }
}
