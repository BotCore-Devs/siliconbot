using System.Collections.Generic;

namespace CoreBot.Structures.DataStorage
{
    public class LegacyContentFilter
    {
        //global toggle
        public bool Enabled;

        //mentions
        public bool MentionsEnabled;
        public int MentionTreshold = 7;
        public int MentionTimeout = 60;

        //invites
        public bool InvitesEnabled;
        public int InviteTreshold = 1;
        public int InviteTimeout = 60;

        //emotes
        public bool EmotesEnabled;
        public int EmoteTreshold = 10;
        public int EmoteTimeout = 60;

        //Bad language
        public bool BadLangEnabled;
        public int BadLangTreshold = 10;
        public int BadLangTimeout = 60;
        public List<string> BadStrings { get; set; } = new List<string>() { "nigger", "nazi", "jew" };

    }
}