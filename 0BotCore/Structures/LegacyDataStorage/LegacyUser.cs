﻿using BotCore.Structures.Base;

using DSharpPlus.Entities;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreBot.Structures.DataStorage
{
    public class LegacyUser
    {
        // global
        public ulong UserId; //user ID
        public string Username = ""; //username
        public string Discriminator = "";
        public string AvatarUrl = "";

        // contexted
        public ulong ServerId;
        public List<string> Nicknames = new List<string>(); //nickname history
        // ??

        [JsonIgnore]
        public LegacyServer Server = null;
        [JsonIgnore]
        public DiscordMember Member => Server == null ? null : Server.Guild.Members.ContainsKey(UserId) ? Server.Guild.Members[UserId] : Server.Guild.GetMemberAsync(UserId).Result;

        public List<string> InvitesUsed = new List<string>();
        public int MessageCount; //message count

        public int Xp;
        public int XpLevel;
        public double XpMultiplier = 1;
        public int XpGoal { get => (XpLevel * 100 + 10) * 25; }
        //public DateTime PreviousMessageTime
        public bool Muted; //is muted? this should become an integer, see Enums.cs > MuteType
        public DateTime UnmuteBy = DateTime.MinValue; //when a user should be auto unmuted
        public int BlockedWordCount; //profanity filter trigger count
        public LegacyContentFilterData ContentFilterData = new LegacyContentFilterData();
        public ulong Credits = 10; //credits for slot machine and daily
        public DateTime LastCreditsRedeem = DateTime.MinValue; //last time credits were redeemed, for <<daily
        public List<string> Warnings = new List<string>(); //list of warnings
        public bool IsStaff => Checks.IsStaff(bot.Client, Server != null ? Server.GuildId:0, UserId); //is staff? (calculated in OnMessage)
        public bool DebugInfo; //debug info logging in chat
        public bool IsPremium => DateTime.Now <= PremiumSince.Add(PremiumDuration);
        public DateTime PremiumSince;
        public TimeSpan PremiumDuration = new TimeSpan(30, 0, 0, 0);

        public bool Left;
        public bool Changed;
        public string CachedLeaderboardCard = "";
        public int CachedLeaderboardIndex;
        public List<QuotedMessage> Quotes = new List<QuotedMessage>();
        private Bot bot;

        public LegacyUser(Bot bot)
        {
            this.bot = bot;
        }

        public void Mute()
        {
            Muted = true;
            UnmuteBy = DateTime.MaxValue;
            Server.MutedUsers.Add(UserId, UnmuteBy);
            Member.GrantRoleAsync(Member.Guild.GetRole(Server.Roles.Muted));
        }
        public void Mute(ulong sec)
        {
            Muted = true;
            UnmuteBy = DateTime.Now.AddSeconds(sec);
            try
            {
                Server.MutedUsers.Add(UserId, UnmuteBy);
            }
            catch
            {
                // ignored
            }

            Member.GrantRoleAsync(Server.Guild.Roles.First(x => x.Value.Name == "Muted").Value);
        }
        public void Unmute()
        {
            Muted = false;
            UnmuteBy = DateTime.MinValue;
            Server.MutedUsers.Remove(UserId);
            Member.RevokeRoleAsync(Server.Guild.Roles.First(x => x.Value.Name == "Muted").Value);
        }
        public void Save()
        {
            this.SaveToJsonFile($"{bot.DataDir}/{ServerId}/{UserId}.json");
        }
    }
    /*public class SmallUser
    {
        [JsonIgnore]
        public ulong UserID;
        public int XP;
        public int XPLevel;
        public int XPGoal;
    }*/
    public class QuotedMessage
    {
        public ulong ServerId;
        public ulong ChannelId;
        public ulong MessageId;
        public string MessageUrl => $"https://discordapp.com/channels/{ServerId}/{ChannelId}/{MessageId}";
        public int Rating;
        public string Text;
    }

}
