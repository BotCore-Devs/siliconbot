﻿using BotCore.Structures.Base;

using DSharpPlus.Entities;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using Omnibot.Structures.LegacyDataStorage;

namespace CoreBot.Structures.DataStorage
{
    public class LegacyServer
    {
        [JsonIgnore]
        public DiscordGuild Guild => GuildId != 0 ? bot.Client.Guilds.ContainsKey(GuildId) ? bot.Client.Guilds[GuildId] : bot.Client.GetGuildAsync(GuildId).Result : null;
        [Key]
        public ulong GuildId = 0;
        public string Prefix = "<<"; //prefix
        public bool ModerationEnabled = true;
        public bool ResetOnLeave;
        public int MessageCount { get { int mc = 0; foreach (LegacyUser user in EnumUsers()) { if(user!=null) mc += user.MessageCount; } return mc; } }

        public bool Lockdown; // lockdown mode (auto mute on join)
        public bool AutoRole = true; //autorole (enable/disable role on join)
        public LegacyContentFilter ContentFilter = new();
        public List<string> BannedNameParts = new(); //auto mute list if one of these is found in username
        public List<string> WordFilter = new(); //profanity list
        public Dictionary<ulong, DateTime> MutedUsers = new(); //muted users, not stored in json
        [JsonIgnore]
        public Dictionary<ulong, LegacyUser> Users = new(); //user data for server
        public Roles Roles = new();
        public Channels Channels = new();
        public Dictionary<string, DiscordInvite> Invites = new();
        public bool LevelsEnabled = true;
        public bool LevelUpMessagesEnabled = true;
        public int MinLevelForLeaderboard = 2;
        public bool RaidDefenseEnabled = false;
        public bool UnknownCommandMessage = true;
        public bool DisabledCommandMessage = true;
        public bool ShouldDMIfPermissionDenied = true;
        public Dictionary<string, ulong> AssignableRoles = new();
        public List<string> DisabledCommands = new();
        public List<ulong> IgnoredChannels = new();
        
        //ticket system
        public List<Ticket> Tickets = new();
        
        private readonly Bot bot;

        public LegacyServer(Bot bot)
        {
            this.bot = bot;
        }

        public LegacyUser GetUser(ulong userId, bool isDm) //returns user object for user id, creates if it doesnt exist
        {
            /* User user;
             string BaseFolder = $"{bot.DataDir}/{GuildID}", Filename = $"{BaseFolder}/{UserID}.json";
             if (!Directory.Exists(BaseFolder)) Directory.CreateDirectory(BaseFolder);
            */
            LegacyUser user = new LegacyUser(bot);
            string baseFolder = $"{bot.DataDir}/{GuildId}", filename = $"{baseFolder}/{userId}.json";
            if (!ulong.TryParse(new FileInfo(filename).Name.Replace(".json", ""), out ulong id))
            {
                id = 0;
            }

            if (!Directory.Exists(baseFolder))
            {
                Directory.CreateDirectory(baseFolder);
            }

            if (!File.Exists(filename))
            {
                user = new LegacyUser(bot)
                {
                    Server = this,
                    ServerId = GuildId,
                    UserId = userId
                };
            }
            else
            {
                JsonConvert.PopulateObject(File.ReadAllText(filename), user);
                //user = Users.GetValueOrDefault(UserID, JsonConvert.DeserializeObject<LegacyUser>(File.ReadAllText(Filename)));
            }

            if (user == null)
            {
                user = new LegacyUser(bot);
            }
            user.Server = this;
            user.ServerId = GuildId;
            user.UserId = id;
            return user;
        }
        public LegacyUser GetUserReadOnly(ulong userId) //returns user object for user id, creates if it doesnt exist
        {
            LegacyUser user = new LegacyUser(bot);
            string baseFolder = $"{bot.DataDir}/{GuildId}", filename = $"{baseFolder}/{userId}.json";
            if (!Directory.Exists(baseFolder))
            {
                Directory.CreateDirectory(baseFolder);
            }
            /*if (!File.Exists(Filename)) user = new LegacyUser()
{
   Server = this,
   ServerID = this.GuildID,
   UserID = UserID,
   Member = isDM ? null : Guild == null ? null : Guild.GetMemberAsync(UserID).Result //dirty patch
};*/

            else
            if (File.Exists(filename))
            {
                JsonConvert.PopulateObject(File.ReadAllText(filename), user);
                //user = Users.GetValueOrDefault(UserID, JsonConvert.DeserializeObject<LegacyUser>());
            }

            if (user == null)
            {
                user = new LegacyUser(bot);
            }

            if (!ulong.TryParse(new FileInfo(filename).Name.Replace(".json", ""), out ulong id))
            {
                id = 0;
            }

            user.UserId = id;
            return user;
        }
        public List<LegacyUser> EnumUsers()
        {
            List<LegacyUser> users = new List<LegacyUser>();
            if (Directory.Exists(bot.DataDir + $"/{GuildId}"))
            {
                Parallel.ForEach(Directory.GetFiles(bot.DataDir + $"/{GuildId}"), (file) =>
            {
                if (ulong.TryParse(new FileInfo(file).Name.Replace(".json", ""), out ulong id))
                {
                    users.Add(GetUserReadOnly(id));
                }
            });
            }

            return users;
        }
        public void Save()
        {
            this.SaveToJsonFile($"{bot.DataDir}/{GuildId}/Config.json");
        }
    }
    public class Roles
    {
        public ulong Muted;
        public ulong Member;
        public ulong Mod;
        public ulong Admin;
    }
    public class Channels
    {
        public ulong Mod;
        public ulong Log;
        public ulong Welcome;
    }
}
