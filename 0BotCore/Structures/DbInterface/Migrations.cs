﻿using CoreBot.Structures.DataStorage;
using SiliconBotCore3;
using System.Linq;

namespace BotCore.Structures.DbInterface
{
    internal class Migrations
    {
        private static readonly bool usedb = false;
        public static void MigrateLegacyUserToDb(LegacyUser user)
        {
            if (usedb)
            {
                using Db db = new Db();
                MigrateLegacyServerToDb(user.Server);
                if (db.GlobalUser.Find((long)user.UserId) == null)
                {
                    db.Add(new GlobalUser() { GlobalUserId = (long)user.UserId });
                }

                if (db.DServerUser.Where(x => x.DUserId == (long)user.UserId && x.DServerId == (long)user.ServerId) == null)
                {
                    db.Add(new DServerUser() { DServerId = (long)user.ServerId, DUserId = (long)user.UserId });
                }
                db.SaveChanges();
            }
        }
        public static void MigrateLegacyServerToDb(LegacyServer server)
        {
            if (usedb)
            {
                using Db db = new Db();
                if (db.Server.Find((long)server.GuildId) == null)
                {
                    db.Add(new Server() { DServerId = (long)server.GuildId, ServerId = (long)server.GuildId });
                    db.SaveChanges();
                }
            }
        }
    }

    internal static class MigrationsExt
    {
        public static void MigrateToDb(this LegacyUser user)
        {
            Migrations.MigrateLegacyUserToDb(user);
        }
        public static void MigrateToDb(this LegacyServer server)
        {
            Migrations.MigrateLegacyServerToDb(server);
        }
    }
}
