﻿using BotCore.Structures.Base;

using CoreBot;
using CoreBot.Structures.DataStorage;

using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BotCore.Structures.ServiceWorkers
{
    public class ContentFilterWorker
    {
        public static Dictionary<(ulong ServerID, ulong UserID), Thread> ContentFilterThreads = new();
        private Bot bot;

        public ContentFilterWorker(Bot bot)
        {
            this.bot = bot;
        }

        public async void LoadServer(ulong serverId)
        {
            List<LegacyUser> susers = (await new LegacyDataStore(bot).GetServer(serverId)).EnumUsers()
                                                                   .Where(x => x != null && x.ContentFilterData.Total >= 1)
                                                                   .ToList();
            foreach (LegacyUser user in susers)
            {
                StartUser(serverId, user.UserId);
            }
        }
        public void StartUser(ulong serverId, ulong userId)
        {
            if (!(ContentFilterThreads.ContainsKey((serverId, userId)) && ContentFilterThreads[(serverId, userId)].IsAlive))
            {
                if (ContentFilterThreads.ContainsKey((serverId, userId))) ContentFilterThreads.Remove((serverId, userId));
                Thread t = new Thread(async () =>
                {
                    LegacyServer s = await bot.DataStore.GetServer(serverId);
                    LegacyUser u = s.GetUser(userId, false);
                    LegacyContentFilter hm = s.ContentFilter;
                    LegacyContentFilterData hd = u.ContentFilterData;
                    //define threads
                    Thread mt = new Thread(() => { while (hd.Mentions > 0) { Thread.Sleep(hm.MentionTimeout * 10); hd.Mentions -= 0.01; u.Save(); } });
                    Thread bt = new Thread(() => { while (hd.BadLang > 0) { Thread.Sleep(hm.BadLangTimeout * 10); hd.BadLang -= 0.01; u.Save(); } });
                    Thread it = new Thread(() => { while (hd.Invites > 0) { Thread.Sleep(hm.InviteTimeout * 10); hd.Invites -= 0.01; u.Save(); } });
                    Thread et = new Thread(() => { while (hd.Emotes > 0) { Thread.Sleep(hm.EmoteTimeout * 10); hd.Emotes -= 0.01; u.Save(); } });
                    //start threads
                    mt.Start();
                    bt.Start();
                    it.Start();
                    et.Start();
                    //block this thread to check if its still running
                    mt.Join();
                    bt.Join();
                    it.Join();
                    et.Join();
                });
                t.Start();
                System.Console.WriteLine($"Started new ContentFilterWorker for {serverId}/{userId}");
                ContentFilterThreads.Add((serverId, userId), t);
            }
        }
    }
}
