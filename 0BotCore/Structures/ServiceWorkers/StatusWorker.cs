﻿using BotCore.Structures.Base;

using CoreBot;

using DSharpPlus.Entities;

using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Threading;

namespace BotCore.Structures.ServiceWorkers
{
    class StatusWorker
    {
        public void StartStatusWorker(Bot bot)
        {
            Thread thread = new Thread(
                async () =>
                {
                    bool running = true;
                    while (running)
                    {
                        Stopwatch statusTiming = bot.Timing.StartTiming("Update Status");
                        DiscordActivity activity = bot.Statusses[RuntimeInfo.Rnd.Next(bot.Statusses.Count)];
                        activity = new DiscordActivity(activity.Name.ApplyVars(bot), activity.ActivityType);
                        Thread statusTimingChecker = new Thread(() =>
                        {
                            while (statusTiming.IsRunning)
                            {
                                if (statusTiming.ElapsedMilliseconds >= 5000)
                                {
                                    bot.Stop();
                                    bot.Start();
                                    statusTiming.Stop();
                                    running = false;
                                }
                            }
                        });
                        statusTimingChecker.Start();
                        Console.WriteLine($"Updating status: {activity.ActivityType} {activity.Name}{activity.CustomStatus}, latency: {bot.Client.Ping}, connections: {(await bot.Client.GetConnectionsAsync()).Count}");
                        try
                        {
                            await bot.Client.UpdateStatusAsync(activity);
                        }
                        catch (WebSocketException)
                        {
                            running = false;
                        }
                        bot.Timing.StopTiming("Update Status");
                        Thread.Sleep(60000);
                    }
                }
            );
            thread.Start();
        }
    }
}
