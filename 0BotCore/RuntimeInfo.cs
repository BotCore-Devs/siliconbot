﻿using Microsoft.EntityFrameworkCore;

using SiliconBotCore3;

using System;

namespace CoreBot
{
    public class RuntimeInfo
    {
        public static bool IsDebug = true;
        public static string Ver => "v3.0.0";
        public static readonly Random Rnd = new Random();

        //legacy data model

        //new db system
        public static Db DataBase = new Db(new DbContextOptionsBuilder<Db>().EnableSensitiveDataLogging().Options);
    }
}
