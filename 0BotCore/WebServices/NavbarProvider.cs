﻿using System.IO;

namespace BotCore.WebServices
{
    internal class NavbarProvider
    {
        public static string GetNavBar()
        {
            return @"<meta charset='UTF-8'>
<link rel='stylesheet' href='/navbar.css'>
<ul id='navbar'>
  <li><a href='/' target='_parent'>Home</a></li>
  <li><a href='/leaderboards' target='_parent'>Leaderboards</a></li>
  <li><a href='/timings' target='_parent'>Timings</a></li>
</ul><div style='height: 60px;'></div>";
        }
    }
}
