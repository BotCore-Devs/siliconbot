﻿using BotCore.Structures.Base;
using CoreBot.Structures.Command;

using System;
using System.Threading.Tasks;
using CoreBot.Structures.DataStorage;

namespace Playground
{
    public class Program
    {
        [MTAThread]
        public static void Main()
        {
            RunBotAsync().GetAwaiter().GetResult();
        }
        public static async Task RunBotAsync()
        {
            Bot bot = new Bot("Playground");
            bot.CommandHandler.RegisterCommand("example", new CommandDefinition(CommandCategory.BuiltIn, "Example command", async (ce) =>
                {
                    await ce.SendMessageAsync("Ping :b:ong!");
                }
            ));
            LegacyServer botDevGuild = (await bot.DataStore.GetServer(643482410339794974));
            botDevGuild.Prefix = "pg!";
            botDevGuild.Save();
            bot.Start();
            await Task.Delay(-1);
        }
    }
}
